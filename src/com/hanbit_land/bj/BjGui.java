package com.hanbit_land.bj;

import javax.swing.JPanel;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

public class BjGui extends JPanel {

	/**
	 * Create the panel.
	 */
	public BjGui() {
		setSize(450, 630);
		setBackground(new Color(0, 128, 128));
		setLayout(null);
		
		
		JButton btnHit = new JButton("HIT");
		btnHit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnHit.setBounds(70, 510, 75, 23);
		add(btnHit);
		
		JButton btnStay = new JButton("STAY");
		btnStay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnStay.setBounds(185, 510, 75, 23);
		add(btnStay);
		
		JButton btnBet = new JButton("100+");
		btnBet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBet.setBounds(304, 510, 75, 23);
		add(btnBet);
		
/////////////////////dealercard slot start////////////////////////		
		
		JLayeredPane dealerCard = new JLayeredPane();
		dealerCard.setBounds(125, 60, 75, 100);
		add(dealerCard);
		
		JLabel dcardImage1 = new JLabel("");
		dcardImage1.setIcon(new ImageIcon(BjGui.class.getResource("")));
		dcardImage1.setBounds(0, 0, 75, 100);
		dealerCard.add(dcardImage1);
		
		JLabel dcardImage1_1 = new JLabel("");
		dcardImage1_1.setIcon(new ImageIcon(BjGui.class.getResource("/bjcardimg/CardBack.png")));
		dcardImage1_1.setBounds(0, 0, 75, 100);
		dealerCard.add(dcardImage1_1);
		
		JLayeredPane dealerCard2 = new JLayeredPane();
		dealerCard2.setBounds(175, 60, 75, 100);
		add(dealerCard2);
		
		JLabel dcardImage2 = new JLabel("");
		dcardImage2.setIcon(new ImageIcon(BjGui.class.getResource("")));
		dcardImage2.setBounds(0, 0, 75, 100);
		dealerCard2.add(dcardImage2);
		
		JLayeredPane dealerCard3 = new JLayeredPane();
		dealerCard3.setBounds(225, 60, 75, 100);
		add(dealerCard3);
		
		JLabel dcardImage3 = new JLabel("");
		dcardImage3.setIcon(new ImageIcon(BjGui.class.getResource("")));
		dcardImage3.setBounds(0, 0, 75, 100);
		dealerCard3.add(dcardImage3);
		
		JLayeredPane dealerCard4 = new JLayeredPane();
		dealerCard4.setBounds(275, 60, 75, 100);
		add(dealerCard4);
		
		JLabel dcardImage4 = new JLabel("");
		dcardImage4.setIcon(new ImageIcon(BjGui.class.getResource("")));
		dcardImage4.setBounds(0, 0, 75, 100);
		dealerCard4.add(dcardImage4);
		
		JLayeredPane dealerCard5 = new JLayeredPane();
		dealerCard5.setBounds(325, 60, 75, 100);
		add(dealerCard5);
		
		JLabel dcardImage5 = new JLabel("");
		dcardImage5.setIcon(new ImageIcon(BjGui.class.getResource("")));
		dcardImage5.setBounds(0, 0, 75, 100);
		dealerCard5.add(dcardImage5);

////////////////////////////dealercard slot end////////////////////////		
////////////////////////////playercard slot start///////////////////

		JLayeredPane playerCard = new JLayeredPane();
		playerCard.setBounds(125, 255, 75, 100);
		add(playerCard);
		
		JLabel pcardImage1 = new JLabel("");
		pcardImage1.setIcon(new ImageIcon(BjGui.class.getResource("")));
		pcardImage1.setBounds(0, 0, 75, 100);
		playerCard.add(pcardImage1);
		
		JLayeredPane playerCard2 = new JLayeredPane();
		playerCard2.setBounds(175, 255, 75, 100);
		add(playerCard2);
		
		JLabel pcardImage2 = new JLabel("");
		pcardImage2.setIcon(new ImageIcon(BjGui.class.getResource("")));
		pcardImage2.setBounds(0, 0, 75, 100);
		playerCard2.add(pcardImage2);
		
		JLayeredPane playerCard3 = new JLayeredPane();
		playerCard3.setBounds(225, 255, 75, 100);
		add(playerCard3);
		
		JLabel pcardImage3 = new JLabel("");
		pcardImage3.setIcon(new ImageIcon(BjGui.class.getResource("")));
		pcardImage3.setBounds(0, 0, 75, 100);
		playerCard3.add(pcardImage3);
		
		JLayeredPane playerCard4 = new JLayeredPane();
		playerCard4.setBounds(275, 255, 75, 100);
		add(playerCard4);
		
		JLabel pcardImage4 = new JLabel("");
		pcardImage4.setIcon(new ImageIcon(BjGui.class.getResource("")));
		pcardImage4.setBounds(0, 0, 75, 100);
		playerCard4.add(pcardImage4);
		
		JLayeredPane playerCard5 = new JLayeredPane();
		playerCard5.setBounds(325, 255, 75, 100);
		add(playerCard5);
		
		JLabel pcardImage5 = new JLabel("");
		pcardImage5.setIcon(new ImageIcon(BjGui.class.getResource("")));
		pcardImage5.setBounds(0, 0, 75, 100);
		playerCard5.add(pcardImage5);

/////////////////////playercard slot end////////////////////
/////////////////////backcard slot start////////////////////

		JLayeredPane backCard = new JLayeredPane();
		backCard.setBounds(25, 160, 75, 100);
		add(backCard);
		
		JLabel backCardImage = new JLabel("");
		backCardImage.setIcon(new ImageIcon(BjGui.class.getResource("/bjcardimg/CardBack.png")));
		backCardImage.setBounds(0, 0, 75, 100);
		backCard.add(backCardImage);

///////////////////////backcard slot end////////////////////////////
///////////////////////resultmsg start//////////////////////////////		
	
		
		//JOptionPane.showMessageDialog(null, "You Win", null, JOptionPane.PLAIN_MESSAGE);
		//JOptionPane.showMessageDialog(null, "You Lose", null, JOptionPane.PLAIN_MESSAGE);
		//JOptionPane.showMessageDialog(null, "Player BackJack", null, JOptionPane.PLAIN_MESSAGE);
		//JOptionPane.showMessageDialog(null, "Dealer BackJack", null, JOptionPane.PLAIN_MESSAGE);
		//JOptionPane.showMessageDialog(null, "Again?", null, JOptionPane.PLAIN_MESSAGE);
		

		
		/*JButton btnPlayerWin = new JButton("You Win");
		btnPlayerWin.setBounds(175, 222, 97, 23);
		add(btnPlayerWin);
		
		JButton btnPlayerLose = new JButton("You Lose");
		btnPlayerLose.setBounds(175, 222, 97, 23);
		add(btnPlayerLose);
		
		JButton btnPlayerBj = new JButton("Player BackJack");
		btnPlayerBj.setBounds(175, 222, 97, 23);
		add(btnPlayerBj);
		
		JButton btnDealerBj = new JButton("Dealer BackJack");
		btnDealerBj.setBounds(175, 222, 97, 23);
		add(btnDealerBj);
		
		JButton AgainGame = new JButton("Again?");
		AgainGame.setBounds(175, 200, 97, 67);
		add(AgainGame);*/

///////////////////////resultmsg end/////////////////////////////////		
	
		
	}
	public static void main(String[] args) {
		JFrame frame=new JFrame();
		JPanel pan=new BjGui();
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(pan);
		frame.setVisible(true);
		
	}
}
