package com.hanbit_land.kjy.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.main.Main;
import com.hanbit_land.kjy.process.MemberDeletePro;
import com.hanbit_land.kjy.process.MemberEditPro;

public class MemberEdit extends JFrame{
	//회원 정보 수정 새창
	MemberInfo memInfo;
	HBLand hbLand;
	HBLandVO hbVO;
	
	//회원 정보 수정 컴포넌트
	public JPanel updateP = new JPanel();
	public JLabel updateBg = new JLabel(new ImageIcon("src/mainImgs/memEditImg.png"));

	public JLabel lbIdEdit = new JLabel();
	public JLabel lbBirthEdit = new JLabel();
	public JTextField tfNameEdit = new JTextField();
	public JTextField tfEmailEdit = new JTextField();
	public JPasswordField pfPwEdit = new JPasswordField();

	public JButton btnEdit = new JButton("수정");
	public JButton btnCancel = new JButton("취소");
	public JButton btnDel = new JButton("탈퇴");
	
	//생성자
	public MemberEdit(MemberInfo memInfo) {
		this.memInfo = memInfo;
		
		setTitle(hbLand.memDTO.getId() + "님의 회원 정보 수정");
		setSize(305, 525);
		setResizable(false);
		setLocationRelativeTo(null);
		setLayout(null);
		setVisible(true);
		
		updateP.setBounds(0, 0, 300, 500);
		add(updateP);
		updateP.setLayout(null);
		updateBg.setBounds(0, 0, 300, 500);
		updateP.add(updateBg);
		
		//id 라벨
		lbIdEdit.setText(hbLand.memDTO.getId());
		lbIdEdit.setForeground(Color.WHITE);
		lbIdEdit.setFont(new Font("돋움", Font.BOLD, 16));
		lbIdEdit.setBounds(135, 115, 120, 30);
		updateBg.add(lbIdEdit);

		//pw 수정 필드
		pfPwEdit.setBounds(135, 165, 120, 30);
		updateBg.add(pfPwEdit);

		//name 수정 필드
		tfNameEdit.setBounds(135, 215, 120, 30);
		tfNameEdit.setText(hbLand.memDTO.getName());
		updateBg.add(tfNameEdit);

		//email 수정 필드
		tfEmailEdit.setBounds(135, 265, 120, 30);
		tfEmailEdit.setText(hbLand.memDTO.getEmail());
		updateBg.add(tfEmailEdit);

		//birth 라벨
		lbBirthEdit.setText(hbLand.memDTO.getBirth());
		lbBirthEdit.setForeground(Color.WHITE);
		lbBirthEdit.setFont(new Font("돋움", Font.BOLD, 16));
		lbBirthEdit.setBounds(135, 315, 120, 30);
		updateBg.add(lbBirthEdit);

		//탈퇴 버튼
		btnDel.setBounds(60, 365, 60, 30);
		btnDel.setFont(new Font("돋움", Font.BOLD, 12));
		btnDel.addActionListener(new MemberDeletePro(this));
		updateBg.add(btnDel);

		// 수정 완료 버튼
		btnEdit.setBounds(125, 365, 60, 30);
		btnEdit.setFont(new Font("돋움", Font.BOLD, 12));
		btnEdit.addActionListener(new MemberEditPro(this));
		updateBg.add(btnEdit);
		
		//수정 취소 버튼
		btnCancel.setBounds(190, 365, 60, 30);
		btnCancel.setFont(new Font("돋움", Font.BOLD, 12));
		btnCancel.addActionListener((e) -> {dispose();});
		updateBg.add(btnCancel);
		
	}
	
}
