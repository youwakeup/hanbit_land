package com.hanbit_land.kjy.process;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.view.MemberEdit;

public class MemberDeletePro implements ActionListener{

	MemberEdit memEdit;
	HBLandVO hbVO;
	HBLand hbLand;
	
	public MemberDeletePro(MemberEdit memEdit) {
		this.memEdit = memEdit;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//탈퇴 버튼
		if(e.getSource().equals(memEdit.btnDel)) {
			int yes = JOptionPane.showConfirmDialog(memEdit.btnDel, "탈퇴 시 코인을 포함한 모든 데이터가 삭제되어 복구가 불가능합니다. 탈퇴 하시겠습니까?", "탈퇴", JOptionPane.YES_NO_OPTION);
			if (yes == JOptionPane.YES_OPTION) { //yes 버튼 눌렀을 때 --> JDialog 생성
				JDialog delD = new JDialog(memEdit, "탈퇴", true);
				delD.setSize(270, 200);
				delD.setLocationRelativeTo(null);
				delD.setLayout(null);
				
				//id & pw 비교
				JTextField delId = new JTextField();
				JPasswordField delPw = new JPasswordField();
				JLabel lbDelId = new JLabel("ID :");
				JLabel lbDelPw = new JLabel("PW :");
				JButton delBtn = new JButton("확인");
				JButton cancelBtn = new JButton("취소");
				lbDelId.setBounds(30, 30, 50, 30);
				lbDelPw.setBounds(30, 70, 50, 30);
				delId.setBounds(70, 30, 130, 30);
				delPw.setBounds(70, 70, 130, 30);
				delBtn.setBounds(70, 110, 60, 30);
				cancelBtn.setBounds(140, 110, 60, 30);
				
					// id & pw가 일치하는 경우 탈퇴 진행
					delBtn.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (delId.getText().equals(hbLand.memDTO.getId())) {
								// 로그인 했던 아이디와 탈퇴 dialog에 기입한 아이디가 일치할 경우
								String id = delId.getText();
								String pw = delPw.getText();
								MemberDTO memDTO = new MemberDAOImp().selectMember(id, pw);
								if (memDTO != null) {
									// DB에 있는 회원 정보(ID/PW)와 해당 사용자의 memDTO가 일치할 경우
									int num = memDTO.getNum();
									int delDTO = new MemberDAOImp().deleteMember(num);
									if (delDTO > 0) {
										JOptionPane.showMessageDialog(null, "탈퇴가 완료 되었습니다. 게임이 종료됩니다.");
										System.exit(0);
									}
								} else {JOptionPane.showMessageDialog(null, "아이디 또는 비밀번호를 확인해주세요.");}
							} else {JOptionPane.showMessageDialog(null, "현재 로그인 한 계정만 탈퇴 할 수 있습니다.");}
						}
					});
					//탈퇴 취소 버튼
					cancelBtn.addActionListener(new ActionListener() {@Override public void actionPerformed(ActionEvent e) {memEdit.dispose();}});
					
				delD.add(lbDelId);
				delD.add(lbDelPw);
				delD.add(delId);
				delD.add(delPw);
				delD.add(delBtn);
				delD.add(cancelBtn);
				delD.setVisible(true);
			}
		}
	}
}