package com.hanbit_land.jds;
import javax.swing.*;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.main.HBLand;

import oracle.net.aso.d;

import java.awt.*;
import java.util.Random;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PokerGUI extends JPanel{

	
	JTextArea ta2;
	public  Dividecard DIVIDE_CARD;
	JButton b1 = new JButton("2배배팅");
	JButton b2 = new JButton("같은배팅");
	JButton b3 = new JButton("다이");
	JButton b4 = new JButton("시작");
	static HBLand land;
	JLabel l2 = new JLabel();
	JLabel l3 = new JLabel();
	public PokerGUI(HBLand land) {//HBLand land
		this.land=land;
		
		setBackground(new Color(51, 153, 153));
		setLayout(null);
		
		l2.setHorizontalAlignment(SwingConstants.CENTER);
		l2.setBounds(325, 21, 130, 150);
		add(l2);
		
		l3.setHorizontalAlignment(SwingConstants.CENTER);
		l3.setBounds(31, 21, 130, 150);
		add(l3);
		
		b1.setBounds(12, 240, 130, 50);
		add(b1);
		
		
		b2.setBounds(168, 240, 130, 50);
		add(b2);
		
		b3.setBounds(310, 240, 130, 50);
		add(b3);
		
		b4.setBounds(188, 21, 97, 50);
		add(b4);
		
		ta2 = new JTextArea();
		DIVIDE_CARD = new Dividecard(this);
		add(ta2);
		
		
		ta2.setBounds(30,300,390,287);
		JScrollPane scrollPane = new JScrollPane(ta2);
		scrollPane.setLocation(30,300);
		scrollPane.setSize(400, 300);
		add(scrollPane);
		this.setBounds(0, 0, 480, 640);
		
		JLabel l5 = new JLabel("내 카드");
		l5.setHorizontalAlignment(SwingConstants.CENTER);
		l5.setBounds(59, 184, 70, 46);
		add(l5);
		
		JLabel l6 = new JLabel("상대방카드");
		l6.setHorizontalAlignment(SwingConstants.CENTER);
		l6.setBounds(335, 181, 70, 46);
		add(l6);
		System.out.println(DIVIDE_CARD);
		
	}
	//result 0 승 1 패 coin 변화량
	void log(int result, int coin) {
	winCheck(result,coin);
	int id = land.memDTO.getNum();
	land.logDTO.setMem_num(id);
	land.logDTO.setGame(3);
	land.logDTO.setCoin(coin);
	land.logDTO.setResult(result);
	new LogDAOImp().insertLog(land.logDTO);	
	}
	//
	void winCheck(int re, int coin) {
		//int totalCoin=Integer.parseInt(land.infoMem.lbCoin.getText());
		int totalCoin=Integer.parseInt(land.hbVO.lbInfoCoin.getText());
		totalCoin+=coin;
		int win=new LogDAOImp().selectResult(0, HBLand.memDTO.getId());
		int lose=new LogDAOImp().selectResult(1, HBLand.memDTO.getId());
		//시작할때 보여주기식으로 100원을 미리 깠으므로
		if(re==0) {//이겼을때
			win++;
			totalCoin+=100;
		}else {//졌을때
			lose++;
			totalCoin+=-100;
		}
		String res = Integer.toString(win)+"/"+Integer.toString(win+lose);
		//land.infoMem.lbRecord.setText(res);
		//land.infoMem.lbCoin.setText(Integer.toString(totalCoin));
		land.hbVO.lbInfoRecord.setText(res);
		land.hbVO.lbInfoCoin.setText(Integer.toString(totalCoin));
	}
	
	
	public static void main(String[] args)  {
		JFrame f = new JFrame();
		f.getContentPane().setLayout(null);
		f.setBounds(300, 300, 480, 640);
		f.setDefaultCloseOperation(3);
		f.getContentPane().add(new PokerGUI(land));
		f.setVisible(true);
		
		
	}
}