package com.hanbit_land.kjy.dao;

import com.hanbit_land.kjy.dto.MemberDTO;

public interface MemberDAO {
	
	public MemberDTO selectMember(String id);							//id 중복 체크, 회원 정보 출력
	public MemberDTO selectMember(String id, String pw); 		//로그인
	public int updateMember(MemberDTO memDTO); 				//회원 정보 수정
	public int insertMember(MemberDTO memDTO); 					//회원 가입
	public int deleteMember(int num); 											//회원 탈퇴
	public int updateInfoImg(String info_img, String id); 			//캐릭터 이미지 수정
}
