package com.hanbit_land.kdh;

public class Card {  //Card Image (Card1, Card2, Card3, Card4 (JLabel))
	int[] card;
	
	public Card(){  //카드섞기
		
		card = new int[20];  //숫자 20생성
		
		for(int i=0; i<20; i++) {  //카드배열에 1씩 숫자 추가
			card[i] = i+1;
		}
		
		for(int i=0; i<20; i++) {  //카드섞는 for문
			int ran = (int) (Math.random() * 20);  //1.랜덤한 숫자생성 0~9 5.랜덤한 숫자생성 0~9  //형변환? 
			int temp = card[i];  //2.카드배열의 숫자를 임시저장   6.바뀐 카드배열을 임시저장
			card[i] = card[ran];  //3.랜덤생성된 카드 배열을 카드배열로 옮김  7.랜덤카드숫자 두번째 자리로 옮김
			card[ran] = temp;  //4.임시저장된 원래 숫자를 랜덤카드배열로 옮김  8.바뀐임시저장 랜덤카드배열로 옮김
		}
	}
	public int End(int a, int b) {// 끗
		int i=0;
		i= (a+b)%10;  //10으로 나누면 1의자리 숫자를 구함 (3이어도 10으로 안나눠져서 3이몫 , 13을 나눠도 3이 몫 그래서 끗을 구함)
		return i;
	}
	
	public int ddang(int a, int b) {// 땡
		int i=0;
		if(a%10 == b%10) { i=a%10; }  //a,b의 몫이 같다면 a몫을 i에 대입
		return i;
	}
	
	public void CardCombo(Player p){  //카드 족보
		int num;// = p.Card1 + p.Card2;  //Player p.Card1 + p.Card2
		
		if(p.Card1 + p.Card2 == 10 || p.Card1 + p.Card2 == 20 || p.Card1 + p.Card2 == 30) {
			num = 0;
			System.out.println("\n"+"===망통 입니다.===");
			p.scoreName = "망통";
		}else if(ddang(p.Card1, p.Card2)==1){
			num = 81;
			System.out.println("\n"+"===1땡 입니다.===");
			p.scoreName = "1땡";
		}else if(p.Card1 == 3 && p.Card2 == 18) {
			num = 100;
			System.out.println("\n"+"===38광땡 입니다.");
			p.scoreName = "38광땡";
		}else if(p.Card1 == 1 && p.Card2 == 18 || p.Card1 == 1 && p.Card2 == 13){
			num = 99;
			System.out.println("\n"+"===13광땡 & 18광땡 입니다.===");
		}else if(ddang(p.Card1, p.Card2)==2) {
			num = 82;
			System.out.println("\n"+"===2땡 입니다.===");
			p.scoreName = "2땡";
		}else if(ddang(p.Card1, p.Card2)==3) {
			num = 83;
			System.out.println("\n"+"===3땡 입니다.===");
			p.scoreName = "3땡";
		}else if(ddang(p.Card1, p.Card2)==4) {
			num = 84;
			System.out.println("\n"+"===4땡 입니다.===");
			p.scoreName = "4땡";
		}else if(ddang(p.Card1, p.Card2)==5) {
			num = 85;
			System.out.println("\n"+"===5땡 입니다.===");
			p.scoreName = "5땡";
		}else if(ddang(p.Card1, p.Card2)==6) {
			num = 86;
			System.out.println("\n"+"===6땡 입니다.===");
			p.scoreName = "6땡";
		}else if(ddang(p.Card1, p.Card2)==7) {
			num = 87;
			System.out.println("\n"+"===7땡 입니다.===");
			p.scoreName = "7땡";
		}else if(ddang(p.Card1, p.Card2)==8) {
			num = 88;
			System.out.println("\n"+"===8땡 입니다.===");
			p.scoreName = "8땡";
		}else if(ddang(p.Card1, p.Card2)==9) {
			num = 89;
			System.out.println("\n"+"===9땡 입니다.===");
			p.scoreName = "9땡";
		}else if(p.Card1 == 10 && p.Card2 == 20 || p.Card1 == 20 && p.Card2 == 10) {
			num = 90;
			System.out.println("\n"+"===장땡 입니다.===");
			p.scoreName = "장땡";
		}else if(p.Card1 == 1 && p.Card2 == 2 || p.Card1 == 11 && p.Card2 == 2 || p.Card1 == 11 && p.Card2 == 12 || p.Card1 == 1 && p.Card2 == 12){
			num = 98;
			System.out.println("\n"+"===알리 입니다.===");
			p.scoreName = "알리";
		}else if(End(p.Card1, p.Card2)==1){
			num = 71;
			System.out.println("\n"+"===1끗 입니다.===");
			p.scoreName = "1끗";
		}else if(End(p.Card1, p.Card2)==2){
			num = 72;
			System.out.println("\n"+"===2끗 입니다.===");
			p.scoreName = "2끗";
		}else if(End(p.Card1, p.Card2)==3){
			num = 73;
			System.out.println("\n"+"===3끗 입니다.===");
			p.scoreName = "3끗";
		}else if(End(p.Card1, p.Card2)==4){
			num = 74;
			System.out.println("\n"+"===4끗 입니다.===");
			p.scoreName = "4끗";
		}else if(End(p.Card1, p.Card2)==5){
			num = 75;
			System.out.println("\n"+"===5끗 입니다.===");
			p.scoreName = "5끗";
		}else if(End(p.Card1, p.Card2)==6){
			num = 76;
			System.out.println("\n"+"===6끗 입니다.===");
			p.scoreName = "6끗";
		}else if(End(p.Card1, p.Card2)==7){
			num = 77;
			System.out.println("\n"+"===7끗 입니다.===");
			p.scoreName = "7끗";
		}else if(End(p.Card1, p.Card2)==8){
			num = 78;
			System.out.println("\n"+"===8끗 입니다.===");
			p.scoreName = "8끗";
		}else if(End(p.Card1, p.Card2)==9){
			num = 79;
			System.out.println("\n"+"===9끗 입니다.===");
			p.scoreName = "9끗";
		}else {
			num = 1;
			System.out.println("\n"+"잡패 입니다???");
			p.scoreName = "이게뭔패지?: "+p.Card1+"  :  "+p.Card2;  //족보에 없는 숫자 확인
		}
		p.score = num;  //족보 점수(num)를 플레이어 p 점수에 대입???
		System.out.println(p.score);
		
	}
}
	

























/*if(num == 0) {
p.scoreName = "망통";
}else if(num == 100) {
p.scoreName = "38광땡";
}else if(num == 81) {
p.scoreName = "1땡";
}else if(num == 82) {
p.scoreName = "2땡";
}else if(num == 83) {
p.scoreName = "3땡";
}else if(num == 84) {
p.scoreName = "4땡";
}else if(num == 85) {
p.scoreName = "5땡";
}else if(num == 86) {
p.scoreName = "6땡";
}else if(num == 87) {
p.scoreName = "7땡";
}else if(num == 88) {
p.scoreName = "8땡";
}else if(num == 89) {
p.scoreName = "9땡";
}else if(num == 90) {
p.scoreName = "장땡";
}else if(num == 99) {
p.scoreName = "13 & 18 광땡";
}else if(num == 98) {
p.scoreName = "알리";
}else if(num == 71) {
p.scoreName = "1끗";
}else if(num == 72) {
p.scoreName = "2끗";
}else if(num == 73) {
p.scoreName = "3끗";
}else if(num == 74) {
p.scoreName = "4끗";
}else if(num == 75) {
p.scoreName = "5끗";
}else if(num == 76) {
p.scoreName = "6끗";
}else if(num == 77) {
p.scoreName = "7끗";
}else if(num == 78) {
p.scoreName = "8끗";
}else if(num == 79) {
p.scoreName = "9끗";
}*/

	/*public void CardPass1(Player p1, Player p2) {  //카드 나눠주기 
		System.out.println("\n"+"==========[1]첫번째 카드를 나눠줍니다.==========");
		p1.Card1 = card[0];
		p2.Card1 = card[1];
	}
	
	public void CardPass2(Player p1, Player p2) {  //카드 나눠주기 
		System.out.println("\n"+"==========[2]두번째 카드를 나눠줍니다.==========");
		p1.Card2 = card[2];
		p2.Card2 = card[3];
	}*/
