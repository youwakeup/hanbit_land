--로그인
conn land_admin/admin1234;

--관리자 로그인
conn land_dba/dba1234

--테이블 생성 --시퀀스(회원번호) 생성 *기본키
create sequence sudda_seq start with 1 increment by 1;

--테이블에 칼럼 추가 (number(1~38))
alter table sudda add(Coin number(38));

--칼럼 타입 변경 alter(바꾸다), modify(수정하다)
alter table sudda modify(inpo varchar(20));

create table sudda(
num number(8) constraint sudda_pk_num primary key,
mem_id varchar2(20) constraint sudda_fk_mem_id references member(id),
inpo varchar2(70),
ranking number(8) not null
);

insert into sudda(
sudda_seq.nextval,
mem_id, inpo, ranking) 
VALUES(
'gun6750',
'안녕하세요 김대현 입니다.',
1
);

insert into sudda (num, mem_id, inpo, ranking) values (sudda_seq.nextval, 'member1', '김대현입니다.', 1);

insert into sudda (sudda_seq.nextval,
'gun6750',
'안녕하세요',
'1' );

-- INSERT INTO member VALUES (member_seq.nextval, 'member3', '1234', '멤버3', 'abc@ac.co', '991234');
-- (순서,갯수 일치해야 간단히 쓸수 있음.)






-- references
--  ==> '참조키', 참조하라는 뜻

-- 제약조건 = CONSTRAINT
-- 종류는 총 5가지
-- NOT NULL / UNIQUE / PRIMARY KEY / FOREIGN KEY / CHECK
-- CONSTRAINT PK_테이블명 PRIMARY KEY(칼럼명)
-- 칼럼명 CONSTRAINT PK_테이블명 PRIMARY KEY








