package com.hanbit_land.kjy.dao;

import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.dto.MemberDTO;

public interface LogDAO {
	
	public int firstJoin(String id);								//회원가입 시 10,000코인 자동 지급
	public int selectFreeCoin(String id); 					//코인 무료 충전
	public LogDTO selectCoin(String id);					//로그인 시 해당 유저가 갖고 있는 총 코인
	public int selectResult(int result, String id);		//로그인 시 해당 유저가 갖고 있는 총 게임 기록
	public int insertLog(LogDTO logDTO);					//게임 Log 기록
	public int deleteLog(String id);							//유저 탈퇴 시 Log 기록 삭제
}
