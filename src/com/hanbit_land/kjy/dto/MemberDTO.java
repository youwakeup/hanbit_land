package com.hanbit_land.kjy.dto;

public class MemberDTO {
	
	/*
  	Name        		Null?    		Type
		----------- 	-------- 		-----------
		NUM				NOT NULL 	NUMBER(8)
		ID									VARCHAR2(20)
		PW    			NOT NULL 	VARCHAR2(20)
		NAME  			NOT NULL 	VARCHAR2(20)
		EMAIL    		NOT NULL 	VARCHAR2(20)
		BIRTH   		NOT NULL 	VARCHAR2(20)
		INFO_IMG  	NOT NULL 	VARCHAR2(30)
 */
	
	private int num;
	private String id;
	private String pw;
	private String name;
	private String email;
	private String birth;
	private String info_img;
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getInfo_img() {
		return info_img;
	}
	public void setInfo_img(String info_img) {
		this.info_img = info_img;
	}
	@Override
	public String toString() {
		return "MemberDTO [num=" + num + ", id=" + id + ", pw=" + pw + ", name=" + name + ", email=" + email
				+ ", birth=" + birth + ", info_img=" + info_img + "]";
	}
}
