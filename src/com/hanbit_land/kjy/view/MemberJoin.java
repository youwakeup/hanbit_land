package com.hanbit_land.kjy.view;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.hanbit_land.kjy.process.MemberJoinPro;

public class MemberJoin extends JFrame{
	//회원가입 새창
	public JPanel backGroundP;
	public JLabel joinImg = new JLabel(new ImageIcon("src/mainImgs/joinImg.png"));
	
	public JTextField tfJoinId = new JTextField();
	public JPasswordField pfJoinPw = new JPasswordField();
	public JTextField tfJoinName = new JTextField();
	public JTextField tfJoinEmail = new JTextField();
	public JTextField tfJoinBirth = new JTextField();
	
	public JButton btnIdCheck = new JButton("√");
	public JButton btnJoin = new JButton("가입");
	public JButton btnJoinCancel = new JButton("취소");
	
	//id 중복 체크 실행 여부 Key
	public int joinFlag = 0;
	
	public MemberJoin() {
		setTitle("한빛 랜드 회원가입");
		setSize(305, 525);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(null);
		
		backGroundP = new JPanel();
		backGroundP.setBounds(0, 0, 305, 525);
		backGroundP.setLayout(null);
		add(backGroundP);
		
		joinImg.setBounds(0, 0, 300, 500);
		backGroundP.add(joinImg);
		
		tfJoinId.setBounds(135, 115, 120, 30);
		joinImg.add(tfJoinId);

		pfJoinPw.setBounds(135, 165, 120, 30);
		joinImg.add(pfJoinPw);

		tfJoinName.setBounds(135, 215, 120, 30);
		joinImg.add(tfJoinName);

		tfJoinEmail.setBounds(135, 265, 120, 30);
		joinImg.add(tfJoinEmail);

		tfJoinBirth.setText("ex) 18880101");
		tfJoinBirth.setBounds(135, 315, 120, 30);
		joinImg.add(tfJoinBirth);
		
		btnIdCheck.setBounds(257, 115, 40, 30);
		btnIdCheck.setFont(new Font("돋움", Font.BOLD, 10));
		btnIdCheck.addMouseListener(new MemberJoinPro(MemberJoin.this));
		joinImg.add(btnIdCheck);
		
		btnJoin.setBounds(135, 362, 60, 30);
		btnJoin.setFont(new Font("돋움", Font.BOLD, 12));
		btnJoin.addMouseListener(new MemberJoinPro(MemberJoin.this));
		joinImg.add(btnJoin);
		
		btnJoinCancel.setBounds(200, 362, 60, 30);
		btnJoinCancel.setFont(new Font("돋움", Font.BOLD, 12));
		btnJoinCancel.addActionListener((e)->{dispose();});
		joinImg.add(btnJoinCancel);
	}
	
}
