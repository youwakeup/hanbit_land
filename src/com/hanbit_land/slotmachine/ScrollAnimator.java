package com.hanbit_land.slotmachine;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ScrollAnimator extends JPanel{
	private ArrayList<BufferedImage> sprites=new ArrayList<BufferedImage>(7);
	private boolean running=false;//시작시 true
	private boolean isDone=true;//종료시 true
	private int y=64;
	private int ySpeed=0;
	private int yMaxSpeed;
	private long prevTime;
	private final int fps=33;//16=60fps 33=30fps
	private int imgHeight=128;
	private int imgWidth=128;
	private int spriteCenterPos;
	BufferedImage g1=null;
	BufferedImage g2=null;
	BufferedImage g3=null;
	private int index=0;
	private Callback back;
	//생성자 및 초기화
	public ScrollAnimator(Callback back) {
		this.back=back;
		prevTime=System.currentTimeMillis();
		spriteCenterPos=imgHeight/2;
		try {
			BufferedImage img=null;
			String fileName=null;
			for(int i=1;i<8;i++) {
				fileName="src/slotImg/icon"+i+".jpg";
				img=ImageIO.read(new File(fileName));
				sprites.add(img);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		index=(int)(Math.random()*6);
		spriteSet();
	}
	//슬롯 값 리턴
	public int getIndex() {
		return index;
		
	}
	//슬롯 회전 시작
	public void start() {
		if(!running) running=true;
		isDone=false;
		yMaxSpeed= (int)(30 + Math.random()*20);
	}
	//슬롯 멈춤버튼
	public void stop() {
		running=false;
	}
	private void isDone() {
		isDone=true;
		back.call(index);
	}
	
	//숫자 list 길이 안에서 돌도록 
	private int reNum(int x) {
		if(x<0) x=6;
		if(x>6) x=0;
		return x;
	}
	//이미지 바꿈
	private void spriteSet() {
		index=reNum(index);
		g1=sprites.get(reNum(index+1));
		g2=sprites.get(index);
		g3=sprites.get(reNum(index-1));
	}
	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		imgWidth=width;
		imgHeight=height/2;
	}
	@Override
	protected void paintComponent(Graphics g) {
		final Graphics2D g2=(Graphics2D)g;		
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());//화면 지우고
		process(g2);
		repaint();
		
	}
	private void process(Graphics g) {		
		if(System.currentTimeMillis()-prevTime>fps) {
			if(y>imgHeight) {
				y=0;
				index++;
				spriteSet();
				//System.out.println("반짝");
			}
			prevTime=System.currentTimeMillis();
			if(running) {
				if(ySpeed<yMaxSpeed)ySpeed++;
				//System.out.println(ySpeed+" running:"+running);
				y+=ySpeed;			
			}else {
				if(ySpeed>9) {
					//System.out.println(ySpeed+" running:"+running);
					ySpeed--;
					y+=ySpeed;
				}else if(y>58&&y<70) {
					ySpeed=0;
					y=spriteCenterPos;
					if(!isDone)isDone();
				}else {
					y+=ySpeed;
				}
			}
		}
		g.drawImage(g1, 0, y-imgHeight,imgWidth,imgHeight, null);
		g.drawImage(g2, 0, y,imgWidth,imgHeight, null);
		g.drawImage(g3, 0, y+imgHeight,imgWidth,imgHeight, null);
		//System.out.println("y:"+y+" index:"+index+" yspeed:"+ySpeed);		
	}
}
