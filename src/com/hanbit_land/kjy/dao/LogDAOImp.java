package com.hanbit_land.kjy.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.util.OracleDBManager;

public class LogDAOImp implements LogDAO{
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	//회원가입 시 10,000 코인 자동 지급
	@Override
	public int firstJoin(String id) {
		int firstJoin = 0;
		String sql = "INSERT INTO log VALUES (log_seq.nextval, (SELECT num FROM member WHERE id=?), 4, 2, 10000, sysdate)";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			firstJoin = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(pstmt, conn);
		}
		return firstJoin;
	}
	
	//코인 무료 충전
	@Override
	public int selectFreeCoin(String id) {
		int coinOK = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date today = new Date();
		String todayStr = sdf.format(today);
		String sql = "SELECT count(indate) FROM log WHERE mem_num=(SELECT num FROM member WHERE id=?) AND result=2"
				+ "AND to_char(indate, 'yyyymmdd')='"+todayStr+"'";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				coinOK = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(rs, pstmt, conn);
		}
		return coinOK;
	}
	
	//로그인 시 해당 유저가 갖고 있는 총 코인
		@Override
		public LogDTO selectCoin(String id) {
			LogDTO coinDTO = new LogDTO();
			String sql = "SELECT sum(coin) sum_coin FROM log WHERE mem_num=(SELECT num FROM member WHERE id=?)";
			try {
				conn = OracleDBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					coinDTO.setCoin(rs.getInt(1));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				OracleDBManager.close(rs, pstmt, conn);
			}
			return coinDTO;
		}
		
		//로그인 시 해당 유저가 갖고 있는 총 게임 기록
		//승 0 패 1
		@Override
		public int selectResult(int result, String id) {
			int resultSelect=0;
			String sql = "SELECT count(result) FROM log WHERE result =? AND mem_num=(SELECT num FROM member WHERE id=?)";
			try {
				conn = OracleDBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, result);
				pstmt.setString(2, id);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					resultSelect=rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				OracleDBManager.close(rs, pstmt, conn);
			}
			return resultSelect;
		}
		
		//게임 log 기록
		@Override
		public int insertLog(LogDTO logDTO) {
			String sql = "insert into log (num, mem_num, game, result, coin, indate)"
					+ "values(log_seq.nextval, ?, ?, ?, ?, sysdate)";	
			int insertLog = 0;
			try {
				conn = OracleDBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, logDTO.getMem_num());
				pstmt.setInt(2, logDTO.getGame());
				pstmt.setInt(3, logDTO.getResult());
				pstmt.setInt(4, logDTO.getCoin());
				insertLog = pstmt.executeUpdate();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}finally {
				OracleDBManager.close(pstmt, conn);
			}
			return insertLog;
		}
	
	//유저 탈퇴 시 log 데이터 삭제
	@Override
	public int deleteLog(String id) {
		int logDel = 0;
		String sql = "DELETE FROM log WHERE mem_num=(SELECT num FROM member WHERE id=?)";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			logDel = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			OracleDBManager.close(pstmt, conn);
		}
		return logDel;
	}
}
