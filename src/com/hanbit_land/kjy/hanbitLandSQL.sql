----------------------------------------------------------------------------------------------------------------
-- 11/18 기준 DB 수정사항
-- info_img 컬럼 타입 변경
-- 이 파일 그대로 복붙 부탁드려요!
----------------------------------------------------------------------------------------------------------------

drop tablespace LAND_DB including contents and datafiles;
create tablespace LAND_DB
datafile 'C:/oraclexe/app/oracle/oradata/XE/LAND_DB.dbf'
size 100m
autoextend on next 10m
maxsize 500m;

commit;

drop user land_dba cascade;
create user land_dba identified by dba1234
default tablespace LAND_DB
quota unlimited on LAND_DB
temporary tablespace TEMP;
grant connect, resource, dba to land_dba;

commit;

conn land_dba/dba1234;

drop table log;
drop sequence log_seq;
drop table member;
drop sequence member_seq;

create sequence member_seq start with 1 increment by 1;
create table member(
	num number(8) constraint member_pk_num primary key,
	id varchar2(20) unique,
	pw varchar2(20) not null,
	name varchar2(20) not null,
	email varchar2(20) not null,
	birth varchar2(20) not null,
	info_img varchar2(30) default 'no_img.png' not null
);

-- game CK ===== 0:슬롯머신 / 1:블랙잭 / 2:섯다 / 3:인디언 포커 / 4:null(로비에 있는 상태)
-- result CK ===== 0:승 / 1:패 / 2:null(게임 기록 없음)

create sequence log_seq start with 1 increment by 1;
create table log(
	num number(8) constraint log_pk_num primary key,
	mem_num number(8) not null constraint log_fk_mem_num references member(num),
	game number(1) not null constraint log_ck_game check(game in(0,1,2,3,4)),
	result number(1) not null constraint log_ck_result check(result in(0,1,2)),
	coin number(8),
	indate date
);

commit;

-- TEST DATA
INSERT INTO member VALUES (member_seq.nextval, 'member1', '1', '테스트멤버1', 'abc@com.co', 19801026, 'userImg1.png');
INSERT INTO member VALUES (member_seq.nextval, 'member2', '1', 'testMem2', 'test@com.co', 19901126, 'userImg2.png');
INSERT INTO member VALUES (member_seq.nextval, 'member3', '1', '테스트멤버3', '1@com.co', 19951226, 'userImg3.png');
INSERT INTO member VALUES (member_seq.nextval, 'member4', '1', '테스트멤버4', '2@com.co', 19940126, 'userImg4.png');
INSERT INTO member VALUES (member_seq.nextval, 'member5', '1', '테스트멤버5', '3@com.co', 19930226, 'userImg5.png');
INSERT INTO member VALUES (member_seq.nextval, 'member6', '1', '테스트멤버6', '4@com.co', 19920326, 'userImg6.png');
INSERT INTO member VALUES (member_seq.nextval, 'member7', '1', '테스트멤버7', '5@com.co', 19910426, 'userImg7.png');
INSERT INTO member VALUES (member_seq.nextval, 'member8', '1', '테스트멤버8', '6@com.co', 19890526, 'userImg8.png');

commit;

-- 회원 가입 했을 때 코인 10,000 / 게임 기록 0승 0패
INSERT INTO log VALUES (log_seq.nextval, 1, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 2, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 4, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 6, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 4, 2, 10000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 8, 4, 2, 10000, sysdate);

commit;

-- mem_num==홀수의 게임 기록 (승률 50%)
INSERT INTO log VALUES (log_seq.nextval, 1, 0, 0, 1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 3, 0, 4000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 1, 1, -2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 2, 1, -3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 1, 3, 1, -4000, sysdate);

INSERT INTO log VALUES (log_seq.nextval, 3, 0, 0, 1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 3, 0, 4000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 1, 1, -2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 2, 1, -3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 3, 3, 1, -4000, sysdate);

commit;

INSERT INTO log VALUES (log_seq.nextval, 5, 0, 0, 1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 3, 0, 4000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 1, 1, -2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 2, 1, -3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 5, 3, 1, -4000, sysdate);

INSERT INTO log VALUES (log_seq.nextval, 7, 0, 0, 1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 3, 0, 4000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 1, 1, -2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 2, 1, -3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 7, 3, 1, -4000, sysdate);

commit;

drop user land_admin cascade;
create user land_admin identified by admin1234
default tablespace LAND_DB
quota unlimited on LAND_DB;

grant connect to land_admin;
grant create synonym to land_admin;
grant select any table to land_admin;
grant delete any table to land_admin;
grant update any table to land_admin;
grant insert any table to land_admin;
grant select any sequence to land_admin;

commit;

conn land_admin/admin1234;

create synonym member for land_dba.member;
create synonym log for land_dba.log;
create synonym member_seq for land_dba.member_seq;
create synonym log_seq for land_dba.log_seq;

commit;

INSERT INTO member VALUES (member_seq.nextval, 'member9', '1', '테스트멤버9', '6@com.co', 19870626, 'userImg5.png');
INSERT INTO log VALUES (log_seq.nextval, 9, 4, 2, 10000, sysdate);

-- mem_num==짝수 게임 기록(승률 100% or 0%...)
INSERT INTO log VALUES (log_seq.nextval, 2, 0, 0, 1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 2, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 2, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 2, 3, 0, 4000, sysdate);

INSERT INTO log VALUES (log_seq.nextval, 4, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 4, 1, 1, -2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 4, 2, 1, -3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 4, 3, 1, -4000, sysdate);

INSERT INTO log VALUES (log_seq.nextval, 6, 0, 1, -1000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 6, 1, 0, 2000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 6, 2, 0, 3000, sysdate);
INSERT INTO log VALUES (log_seq.nextval, 6, 3, 0, 4000, sysdate);

commit;




