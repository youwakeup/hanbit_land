package com.hanbit_land.kjy.dto;

import java.util.Date;

public class LogDTO {
	/*
	Name		Null?		Type
		--------- 	-------- 	--------
		NUM					NOT NULL 	NUMBER(8)
		MEM_NUM		NOT NULL	NUMBER(8)
		GAME				NOT NULL 	NUMBER(1)
		RESULT			NOT NULL 	NUMBER(1)
		COIN									NUMBER(8)
		INDATE								DATE
		//Date == java.util.Date;
 */

	private int num;
	private int mem_num;
	private int game;
	private int result;
	private int coin;
	private Date indate;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getMem_num() {
		return mem_num;
	}

	public void setMem_num(int mem_num) {
		this.mem_num = mem_num;
	}

	public int getGame() {
		return game;
	}

	public void setGame(int game) {
		this.game = game;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getCoin() {
		return coin;
	}

	public void setCoin(int coin) {
		this.coin = coin;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	@Override
	public String toString() {
		return "LogDTO [num=" + num + ", mem_num=" + mem_num + ", game=" + game + ", result=" + result + ", coin="
				+ coin + ", indate=" + indate + "]";
	}
}
