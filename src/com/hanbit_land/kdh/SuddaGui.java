﻿package com.hanbit_land.kdh;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.main.HBLand;

public class SuddaGui extends JPanel{
	
	int playcoin = 0;  //판돈
	int callBet = 100;  //콜배팅 내기
	int ddadangBet = 200;  //과감한 200원 배팅
	int win;
	int lose;
	
	 JTextField chatting_tf;
	 JTextArea chatting_ta,myInfo_ta,info_ta;
	 JLabel player1_lb,player2_lb,card1_lb,card2_lb,card3_lb,card4_lb,throwAway_lb,cards_lb;
	 JButton char1_bt,char2_bt,char3_bt,char4_bt,char5_bt,char6_bt,call_bt,ddadang_bt,allin_bt,die_bt
	 		,p1_pedigree_bt,p2_pedigree_bt,p2_call_bt,p2_allin_bt,p2_die_bt,p2_ddadang_bt
	 		,pedigree_bt;
	 JScrollPane info_ta_pane, chatting_ta_pane, myInfo_ta_pane;
	 JLabel p1_lb, p2_lb, bettingInfo_lb;
	 
	 HBLand land;
	 Play play;
	 //MainSound mainSound;
	 
	public SuddaGui(HBLand hbLand) {
		land=hbLand;
		play = new Play(Integer.parseInt(HBLand.hbVO.lbInfoCoin.getText()));
		
		//MainSound mainSound = new MainSound("Kalimba.mp3", true);
		//mainSound.start();
		
		setForeground(new Color(0, 0, 0));
		setBackground(new Color(51, 153, 153));  //배경색
		setLayout(null);

		player2_lb = new JLabel("player2");
		player2_lb.setBounds(227, 24, 42, 74);
		add(player2_lb);
		
		player1_lb = new JLabel("player1");
		player1_lb.setBounds(244, 535, 42, 74);
		add(player1_lb);
		
		card1_lb = new JLabel();  //카드이미지
		card1_lb.setBounds(203, 462, 50, 77);
		add(card1_lb);
		
		card2_lb = new JLabel("card2");
		card2_lb.setBounds(261, 462, 50, 75);
		add(card2_lb);
		
		card3_lb = new JLabel("card3");  
		card3_lb.setBounds(203, 110, 50, 75);
		add(card3_lb);
		
		card4_lb = new JLabel("card4");
		card4_lb.setBounds(261, 110, 50, 75);
		add(card4_lb);
		
		cards_lb = new JLabel("cards");
		cards_lb.setIcon(new ImageIcon("suddaImg/suddaImg/cardBack.PNG"));
		cards_lb.setBounds(227, 278, 48, 68);
		add(cards_lb);
		
		chatting_tf = new JTextField();  //채팅입력창
		chatting_tf.setBounds(321, 449, 147, 21);
		chatting_tf.setColumns(10);
		chatting_tf.addActionListener(new ActionListener() {  //채팅이벤트
			@Override
			public void actionPerformed(ActionEvent e) {
				chatting_ta.append(chatting_tf.getText()+"\n");
				chatting_tf.setText("");
			}
		});
		add(chatting_tf);
		
		chatting_ta = new JTextArea();  //채팅창
		chatting_ta_pane = new JScrollPane(chatting_ta);  //스크롤팬에 채팅창 올린다.
		chatting_ta.setBounds(321, 154, 147, 297);  //채팅창 위치,사이즈
		chatting_ta_pane.setBounds(321, 154, 147, 296);  //스크롤팬 위치,사이즈
		chatting_ta.setCaretPosition(chatting_ta.getDocument().getLength());  //내려가기
		chatting_ta.setFocusable(false);
		add(chatting_ta_pane);  //패널에 스크롤팬 올린다.
		
		info_ta = new JTextArea();  //게임정보창 생성후 써라!
		info_ta_pane = new JScrollPane(info_ta);  //그다음에 스크롤펜 생성
		info_ta.setBounds(12, 154, 178, 327);
		info_ta_pane.setBounds(12, 154, 179, 327);
		info_ta.setCaretPosition(info_ta.getDocument().getLength());  //내려가야되는대???
		add(info_ta_pane);  //add는 맨 나중에!!!
		
		//내 정보창
		myInfo_ta = new JTextArea();
		myInfo_ta_pane = new JScrollPane(myInfo_ta);
		myInfo_ta.setBounds(12, 495, 179, 110);
		myInfo_ta_pane.setBounds(12, 495, 179, 110);  //가로, 세로
		myInfo_ta.setCaretPosition(myInfo_ta.getDocument().getLength());  //내려가기
		add(myInfo_ta_pane);
		
		////콜버튼 세팅
		call_bt = new JButton("콜");
		call_bt.setBounds(321, 491, 69, 54);
		call_bt.addMouseListener(new MouseListener() {  //마우스리스너는 안쓰는 기능도 놔둬야 되는건가?
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {
				bettingInfo_lb.setText("bettingInfo");  //기본텍스트 돌리기
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				call_bt.setCursor(new Cursor(Cursor.HAND_CURSOR));  //손가락모양 만들기
				bettingInfo_lb.setText("콜: -100coin");  //텍스트 바꾸기
			}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		call_bt.addActionListener((e->{call();}));
		add(call_bt);
		///콜버튼 세팅 끝
		
		//따당버튼 세팅
		int i = 0;
		/*switch(i++<3) {
			
		}*/
		ddadang_bt = new JButton("따당");
		ddadang_bt.setBounds(399, 491, 69, 54);
		ddadang_bt.addMouseListener(new MouseListener() {  //마우스리스너는 안쓰는 기능도 놔둬야 되는거라는군
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {
				bettingInfo_lb.setText("bettingInfo");  //기본텍스트 돌리기
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				ddadang_bt.setCursor(new Cursor(Cursor.HAND_CURSOR));  //손가락모양 만들기
				bettingInfo_lb.setText("따당: -200coin");  //텍스트 바꾸기
			}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		ddadang_bt.addActionListener((e->{ddadangBet();}));
		add(ddadang_bt);
		//따당버튼 세팅 끝
		
		allin_bt = new JButton("올인");
		allin_bt.setBounds(321, 551, 69, 54);
		allin_bt.addMouseListener(new MouseListener() {  //마우스리스너는 안쓰는 기능도 놔둬야 되는건가?
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {
				bettingInfo_lb.setText("bettingInfo");  //기본텍스트 돌리기
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				call_bt.setCursor(new Cursor(Cursor.HAND_CURSOR));  //손가락모양 만들기
				bettingInfo_lb.setText("올인: -"+play.p1.coin+"coin");  //텍스트 바꾸기
			}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		allin_bt.addActionListener((e->{allInBet();}));
		add(allin_bt);
		
		/////////////////////////// die 버튼 (게임 끝내기 & 게임 재시작) 그리고 코인 정산!
		die_bt = new JButton("다이");  //"다"+"\n"+"이"줄바꿈???
		die_bt.setBounds(399, 551, 69, 54);
		die_bt.addActionListener((e)->{
			die();
		});
		add(die_bt);
		//die버튼 세팅 끝
		
		pedigree_bt = new JButton("카드오픈");
		pedigree_bt.setBounds(10, 115, 90, 30);
		add(pedigree_bt);
		
		p1_pedigree_bt = new JButton("p1족보");
		p1_pedigree_bt.setBounds(210, 430, 90, 30);
		p1_pedigree_bt.addActionListener((e)->{  //족보보기 버튼
			play.card.CardCombo(play.p1);  //콘솔 출력//매개변수 까지 받아와야 되는구나? 
			p1_pedigree_bt.setText(play.p1.scoreName);  //카드점수에 이름을 넣어서 텍스트에 출력
		});
		add(p1_pedigree_bt);
		
		p2_pedigree_bt = new JButton("p2족보");
		p2_pedigree_bt.setBounds(210, 190, 90, 30);
		p2_pedigree_bt.addActionListener((e)->{  //족보보기 버튼
			play.card.CardCombo(play.p2);  //콘솔 출력//매개변수 까지 받아와야 되는구나? 
			p2_pedigree_bt.setText(play.p2.scoreName);  //카드점수에 이름을 넣어서 텍스트에 출력
		});
		add(p2_pedigree_bt);
		
		char1_bt = new JButton("Char1");  //버튼1 생성
		char1_bt.setBounds(23, 10, 42, 43);  //버튼1 위치
		char1_bt.setIcon(new ImageIcon("suddaImg/suddaChar/woman1.PNG"));
		char1_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player2_lb.setIcon(new ImageIcon("suddaImg/suddaChar/woman1.PNG"));
				info_ta.append("\np2: 이모.PNG");
			}
		});
		add(char1_bt);  //패널위에 얹는다
		
		char2_bt = new JButton("Char2");  //캐릭터 선택 버튼
		char2_bt.setBounds(77, 10, 42, 43);
		char2_bt.setIcon(new ImageIcon("suddaImg/suddaChar/woman2.PNG"));
		char2_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player2_lb.setIcon(new ImageIcon("suddaImg/suddaChar/woman2.PNG"));
				info_ta.append("\np2: 황아줌마.PNG");
			}
		});
		add(char2_bt);
		
		char3_bt = new JButton("Char3");
		char3_bt.setBounds(131, 10, 42, 43);
		char3_bt.setIcon(new ImageIcon("suddaImg/suddaChar/woman3.PNG"));
		char3_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player2_lb.setIcon(new ImageIcon("suddaImg/suddaChar/woman3.PNG"));
				info_ta.append("\np2: 여학생.PNG");
			}
		});
		add(char3_bt);
		
		char4_bt = new JButton("char4");
		char4_bt.setBounds(23, 63, 42, 43);
		char4_bt.setIcon(new ImageIcon("suddaImg/suddaChar/men1.PNG"));
		char4_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player1_lb.setIcon(new ImageIcon("suddaImg/suddaChar/men1.PNG"));
				info_ta.append("\np1: 그냥남자.PNG");
			}
		});
		add(char4_bt);
		
		char5_bt = new JButton("char5");
		char5_bt.setBounds(77, 63, 42, 43);
		char5_bt.setIcon(new ImageIcon("suddaImg/suddaChar/men2.PNG"));
		char5_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player1_lb.setIcon(new ImageIcon("suddaImg/suddaChar/men2.PNG"));
				info_ta.append("\np1: 곱슬남자.PNG");
			}
		});
		add(char5_bt);
		
		char6_bt = new JButton("char6");
		char6_bt.setBounds(131, 63, 42, 43);
		char6_bt.setIcon(new ImageIcon("suddaImg/suddaChar/men3.PNG"));
		char6_bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player1_lb.setIcon(new ImageIcon("suddaImg/suddaChar/men3.PNG"));
				info_ta.append("\np1: 흑형.PNG");
			}
		});
		add(char6_bt);
		
		p2_lb = new JLabel("p2 :");  //그냥라벨
		p2_lb.setBounds(0, 24, 57, 15);
		add(p2_lb);
		
		p1_lb = new JLabel("p1 :");
		p1_lb.setBounds(0, 77, 57, 15);
		add(p1_lb);
		
		bettingInfo_lb = new JLabel("bettingInfo");
		bettingInfo_lb.setBounds(360, 590, 120, 53);
		add(bettingInfo_lb);
		
		////////////////////////////////////////////////////////////////////
		/////////////////////////// 실행/////////////////////////////////////
		////////////////////////////////////////////////////////////////////
		chatting_ta.append("콜: 100coin \n따당: 200coin \n");
		chatting_ta.append("올인: Allin \n다이: 죽음");
	    info_ta.append(play.msg1+"\n");
		info_ta.append("\n"+play.msg);
		info_ta.append("\n"+play.p1.name+"님 입장.");
		info_ta.append("\n"+play.p2.name+"님 입장.\n");
		
		//////////////////////////////////////////첫번째 카드 + 코인 출력
		info_ta.append("\n"+play.msg2);
		info_ta.append("\n"+play.p1.name+"님 시작합니다!\n");
		info_ta.append("\n"+play.msg3+"\n");
		info_ta.append("\n*** "+play.p1.name+"님 배팅 시작! ***\n");
		
		//////////////////////////////////////////p1.Card1 이미지 삽입!
		String url="suddaImg/suddaImg/card"+play.p1.Card1+".jpg";
		card1_lb.setIcon(new ImageIcon(url));
		
		//////////////////////////////////////////p2.Card1 이미지 삽입!
		String ur2="suddaImg/suddaImg/card"+play.p2.Card1+".jpg";
		card3_lb.setIcon(new ImageIcon(ur2));
		
		//////////////////////////////////////////두번째 카드 + 코인 출력
		pedigree_bt.addActionListener((e)->{  //두번째 카드 오픈, 프린트 출력
		info_ta.append("\n"+play.msg4+"\n");
		/*run(1000);
		info_ta.append("\n파이널카드 5초후 공개!");  5,4,3,2,1 카운트 다운 실시간으로 하고싶음.
		run(1000);
		info_ta.append("\n파이널카드 4초후 공개!");
		run(1000);
		info_ta.append("\n파이널카드 3초후 공개!");
		run(1000);
		info_ta.append("\n파이널카드 2초후 공개!");
		run(1000);
		info_ta.append("\n파이널카드 1초후 공개!");*/
		myInfo_ta.append(play.p1.name+"님  coin: "+play.p1.coin+"\n");
		myInfo_ta.append("p1 카드1: "+play.p1.Card1+"  &  ");
		myInfo_ta.append("p1 카드2: "+play.p1.Card2+"\n");
		myInfo_ta.append("카드족보: "+"\n");
		myInfo_ta.append(play.p2.name+"님  coin: "+play.p2.coin+"\n");
		myInfo_ta.append("p1 카드1: "+play.p2.Card1+"  &  ");
		myInfo_ta.append("p1 카드2: "+play.p2.Card2+"\n");
		
		//////////////////////////////////////////p1.Card2 이미지 삽입!
		String ur3="suddaImg/suddaImg/card"+play.p1.Card2+".jpg";
		card2_lb.setIcon(new ImageIcon(ur3));
		//////////////////////////////////////////p2.Card2 이미지 삽입!
		String ur4="suddaImg/suddaImg/card"+play.p2.Card2+".jpg";
		card4_lb.setIcon(new ImageIcon(ur4));
		});
		
		throwAway_lb = new JLabel();
		throwAway_lb.setBounds(325, 10, 350, 120);
		throwAway_lb.setIcon(new ImageIcon("suddaImg/suddaImg/throwAway1.PNG"));
		add(throwAway_lb);
		
		
		start();
	}
	
	
	
	
	//메인 흐름
	private void start() {
		
	}
	
	//콜버튼 눌렀을때
	private void call() {  //콜 배팅
		if(play.p1.coin > 0) {
		info_ta.append("\n"+play.p1.name+"님 콜!!!");
		play.p1.coin -= callBet;
		//라벨셋
		HBLand.hbVO.lbInfoCoin.setText(Integer.toString(play.p1.coin));  //라벨에 실시간으로 내돈표시
		info_ta.append("\n"+play.p1.name+"님이 "+callBet+"배팅하셨습니다\n");
		playcoin +=callBet;  //판돈 증가
		info_ta.append("전체판돈은: "+playcoin+"coin 입니다\n");
		}else {
			info_ta.append("\n"+play.p1.name+"님 코인이 없습니다.");
			info_ta.append("\n 충전후 다시오세요 ^^");
		}
	}
	
	private void ddadangBet() {  //따당 배팅
		if(play.p1.coin > 0) {
			info_ta.append("\n"+play.p1.name+"님 따당!!");
			play.p1.coin -= ddadangBet;
			//라벨셋
			HBLand.hbVO.lbInfoCoin.setText(Integer.toString(play.p1.coin));  //라벨에 실시간으로 내돈표시
			info_ta.append("\n"+play.p1.name+"님이 "+ddadangBet+"배팅하셨습니다\n");
			playcoin += ddadangBet;  //판돈 증가
			info_ta.append("전체판돈은: "+playcoin+"coin 입니다\n");
			}else {
				info_ta.append("\n"+play.p1.name+"님 코인이 없습니다.");
				info_ta.append("\n 충전후 다시오세요 ^^");
			}
	}
	private void allInBet() {  //올인!!
		if(play.p1.coin > 0) {
			info_ta.append("\n"+play.p1.name+"님 올인!!!");
			playcoin += play.p1.coin;  //판돈 증가후
			play.p1.coin -= play.p1.coin;  //돈 0원 만들기
			//라벨셋
			HBLand.hbVO.lbInfoCoin.setText(Integer.toString(play.p1.coin));  //라벨에 실시간으로 내돈표시
			info_ta.append("\n"+play.p1.name+"님이 "+play.p1.coin+"(올인) 하셨습니다\n");
			info_ta.append("전체판돈은: "+playcoin+"coin 입니다\n");
			}else {
				info_ta.append("\n"+play.p1.name+"님 코인이 없습니다.");
				info_ta.append("\n 충전후 다시오세요 ^^");
			}
	}
	private void die() {
		//JFrame f = new JFrame();  //새 창을 띄운다. 안써도 띄워지네?
		int response = JOptionPane.showConfirmDialog(null, "승패 결과를 보겠습니까?", "",
		JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		// System.out.println(response);		// ok는 0, cancle은 1 반환
		
		if(response==0) {  //다이 1번 버튼  // 새로 시작 만들기
			play.card.CardCombo(play.p1);  //플레이어1의 카드 숫자를 받아와서 
			play.card.CardCombo(play.p2);  //플레이어2의 카드 숫자도 받아온다
		
			//1p승리
			if(play.p1.score > play.p2.score) {  //그리고 플레이어 클래스의 스코어를 대입후 비교후 승패 결정
			info_ta.append("\np1: "+play.p1.name+"님 승리 ~ !!!!!!");
			info_ta.append("\n"+"이제 나가주세요.");
			play.p2.coin -= playcoin/2;
			play.p1.coin += playcoin;
			//setLogToDB(hbLand,0);  //db에 진짜 찍힌다.
			//hbLand.infoMem.lbCoin.setText(Integer.toString(play.p1.coin));  //문자로 형변환?
			
			//승패기록
			
			}else {
			//2p 승리시
			info_ta.append("\np2: "+play.p2.name+"님 승리 ~ !!!!!!");
			info_ta.append("\n"+"이제 나가주세요.");
			//land.logDTO.setCoin(play.p1.coin);
			//hbLand.logDTO.setResult(hbLand.logDTO.getResult()+0);
			//land.infoMem.lbCoin.setText(Integer.toString(play.p1.coin));  //문자로 형변환?
			//setLogToDB(hbLand,1);  //db에 진짜 찍힌다
			
			//HBLand.logDTO.setMem_num(HBLand.memDTO.getNum());
			//HBLand.logDTO.setGame(3);
			//HBLand.logDTO.setResult(1);
			//win = new LogDAOImp().insertLog(HBLand.logDTO);
			//lose = new LogDAOImp().insertLog(HBLand.logDTO);
			//String re = Integer.toString(win)+"/"+Integer.toString(lose);
			//land.infoMem.lbRecord.setText(re);  //라벨에 승패 출력
			
			//승패기록
			//int win=new LogDAOImp().selectResult(0, HBLand.memDTO.getId());
			//int lose=new LogDAOImp().selectResult(1, HBLand.memDTO.getId());
			//String re = Integer.toString(win)+"/"+Integer.toString(win+lose);
			//land.infoMem.lbRecord.setText(Integer.toString(win)+"/"+Integer.toString(lose));  //졌을때
			}
		
		}else {	//다이 2번버튼	//게임 종료 만들기
		System.exit(0);  //이거 숫자는 뭐임?
		}
	}
	
	private void cardCheck() {}
	
	private void setLogToDB(HBLand land,int result) {
		if(result==0){  //이겼을때 
			land.logDTO.setCoin(play.p1.coin);
			land.logDTO.setResult(0);
		}else{  //졌을때
			land.logDTO.setCoin(-play.p1.coin);
		}
		land.logDTO.setResult(result);
		//result = 0;
		land.logDTO.setGame(3);
		land.logDTO.setMem_num(land.memDTO.getNum());
		new LogDAOImp().insertLog(land.logDTO);
	}
	
	private void run(int a){  //몇초 후 잠깐 대기!!
		try {
			Thread.sleep(a);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


















