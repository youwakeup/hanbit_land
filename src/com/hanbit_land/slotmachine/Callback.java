package com.hanbit_land.slotmachine;

public interface Callback {
	public void call(int value);
}
