package com.hanbit_land.kjy.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.main.Main;
import com.hanbit_land.kjy.process.MemberEditImgPro;

public class MemberEditImg extends JFrame{
	//회원 이미지 수정 새창
	
	public MemberInfo memInfo;
	
	public JPanel backGroundP = new JPanel();
	
	public JLabel userImg1 = new JLabel(new ImageIcon("src/mainImgs/userImg1.png"));
	public JLabel userImg2 = new JLabel(new ImageIcon("src/mainImgs/userImg2.png"));
	public JLabel userImg3 = new JLabel(new ImageIcon("src/mainImgs/userImg3.png"));
	public JLabel userImg4 = new JLabel(new ImageIcon("src/mainImgs/userImg4.png"));
	public JLabel userImg5 = new JLabel(new ImageIcon("src/mainImgs/userImg5.png"));
	public JLabel userImg6 = new JLabel(new ImageIcon("src/mainImgs/userImg6.png"));
	public JLabel userImg7 = new JLabel(new ImageIcon("src/mainImgs/userImg7.png"));
	public JLabel userImg8 = new JLabel(new ImageIcon("src/mainImgs/userImg8.png"));
	
	public JRadioButton btnRadio1 = new JRadioButton("선택");
	public JRadioButton btnRadio2 = new JRadioButton("선택");
	public JRadioButton btnRadio3 = new JRadioButton("선택");
	public JRadioButton btnRadio4 = new JRadioButton("선택");
	public JRadioButton btnRadio5 = new JRadioButton("선택");
	public JRadioButton btnRadio6 = new JRadioButton("선택");
	public JRadioButton btnRadio7 = new JRadioButton("선택");
	public JRadioButton btnRadio8 = new JRadioButton("선택");
	
	public JButton btnChangeImg = new JButton("변경");
	public JButton btnCancelImg = new JButton("취소");
	
	public MemberEditImg(MemberInfo memInfo) {
		this.memInfo = memInfo;
		
		setTitle("캐릭터 이미지 변경");
		setSize(720, 520);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(null);
		
		backGroundP.setBackground(Color.DARK_GRAY);
		backGroundP.setBounds(0, 0, 720, 520);
		backGroundP.setLayout(null);
		add(backGroundP);
		
		//이미지 라벨 설정
		userImg1.setBounds(30, 30, 150, 150);
		userImg1.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg1);
		
		userImg2.setBounds(200, 30, 150, 150);
		userImg2.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg2);
		
		userImg3.setBounds(370, 30, 150, 150);
		userImg3.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg3);
		
		userImg4.setBounds(540, 30, 150, 150);
		userImg4.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg4);
		
		userImg5.setBounds(30, 240, 150, 150);
		userImg5.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg5);
		
		userImg6.setBounds(200, 240, 150, 150);
		userImg6.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg6);
		
		userImg7.setBounds(370, 240, 150, 150);
		userImg7.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg7);
		
		userImg8.setBounds(540, 240, 150, 150);
		userImg8.setBorder(new LineBorder(Color.WHITE, 1));
		backGroundP.add(userImg8);
		
		//라디오 버튼 설정
		btnRadio1.setBackground(null);
		btnRadio1.setBorderPainted(false);
		btnRadio1.setFocusPainted(false);
		btnRadio1.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio1.setForeground(Color.WHITE);
		btnRadio1.setBounds(30, 185, 150, 20);
		backGroundP.add(btnRadio1);
		
		btnRadio2.setBackground(null);
		btnRadio2.setBorderPainted(false);
		btnRadio2.setFocusPainted(false);
		btnRadio2.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio2.setForeground(Color.WHITE);
		btnRadio2.setBounds(200, 185, 150, 20);
		backGroundP.add(btnRadio2);
		
		btnRadio3.setBackground(null);
		btnRadio3.setBorderPainted(false);
		btnRadio3.setFocusPainted(false);
		btnRadio3.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio3.setForeground(Color.WHITE);
		btnRadio3.setBounds(370, 185, 150, 20);
		backGroundP.add(btnRadio3);
		
		btnRadio4.setBackground(null);
		btnRadio4.setBorderPainted(false);
		btnRadio4.setFocusPainted(false);
		btnRadio4.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio4.setForeground(Color.WHITE);
		btnRadio4.setBounds(540, 185, 150, 20);
		backGroundP.add(btnRadio4);
		
		btnRadio5.setBackground(null);
		btnRadio5.setBorderPainted(false);
		btnRadio5.setFocusPainted(false);
		btnRadio5.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio5.setForeground(Color.WHITE);
		btnRadio5.setBounds(30, 395, 150, 20);
		backGroundP.add(btnRadio5);
		
		btnRadio6.setBackground(null);
		btnRadio6.setBorderPainted(false);
		btnRadio6.setFocusPainted(false);
		btnRadio6.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio6.setForeground(Color.WHITE);
		btnRadio6.setBounds(200, 395, 150, 20);
		backGroundP.add(btnRadio6);
		
		btnRadio7.setBackground(null);
		btnRadio7.setBorderPainted(false);
		btnRadio7.setFocusPainted(false);
		btnRadio7.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio7.setForeground(Color.WHITE);
		btnRadio7.setBounds(370, 395, 150, 20);
		backGroundP.add(btnRadio7);
		
		btnRadio8.setBackground(null);
		btnRadio8.setBorderPainted(false);
		btnRadio8.setFocusPainted(false);
		btnRadio8.setHorizontalAlignment(SwingConstants.CENTER);
		btnRadio8.setForeground(Color.WHITE);
		btnRadio8.setBounds(540, 395, 150, 20);
		backGroundP.add(btnRadio8);
		
		ButtonGroup btnGroup = new ButtonGroup();
		btnGroup.add(btnRadio1);
		btnGroup.add(btnRadio2);
		btnGroup.add(btnRadio3);
		btnGroup.add(btnRadio4);
		btnGroup.add(btnRadio5);
		btnGroup.add(btnRadio6);
		btnGroup.add(btnRadio7);
		btnGroup.add(btnRadio8);
		
		//변경 / 취소 버튼
		btnChangeImg.setBounds(300, 450, 60, 30);
		btnChangeImg.addActionListener(new MemberEditImgPro(MemberEditImg.this));
		backGroundP.add(btnChangeImg);
		
		//취소 버튼
		btnCancelImg.setBounds(370, 450, 60, 30);
		btnCancelImg.addActionListener((e)->{dispose();});
		backGroundP.add(btnCancelImg);
	}
}
