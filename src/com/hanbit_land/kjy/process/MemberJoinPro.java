package com.hanbit_land.kjy.process;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.view.MemberJoin;

public class MemberJoinPro extends MouseAdapter {

	MemberJoin memJoin;
	private String checkText = "";

	//생성자
	public MemberJoinPro(MemberJoin memJoin) {
		this.memJoin = memJoin;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		//id 유효성 검사(정규 표현식 사용)
		//String idStrCheck = "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]*$";
		//[a-zA-Z0-9]{1}		: n개의 문자열 중 a~z, A~Z, 0~9 중 무조건 한 글자는 써야함
		//[a-zA-Z0-9_]* 		: a~z, A~Z, 0~9 중 없음 또는 한 개 이상
		
		//id 중복 체크 버튼 클릭
		if(e.getSource().equals(memJoin.btnIdCheck)) { 
			String id = memJoin.tfJoinId.getText();
			if(!id.equals(checkText)) { //id가 공란이 아님
				if(id.indexOf(" ") < 0) { //id에 공백이 없음
					String idStrCheck = "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]*$";
					boolean idTrue = Pattern.matches(idStrCheck, id);
					if(idTrue){ //id가 영문, 숫자로만 이루어져 있음
			 			MemberDTO memDTO = new MemberDAOImp().selectMember(id);
							if (memDTO.getId() == null) { //DB와 연결 후 ID 중복이 아님
								JOptionPane.showMessageDialog(memJoin, "사용 가능한 ID입니다.");
								memJoin.joinFlag = 1;
							} else { JOptionPane.showMessageDialog(memJoin, "사용 불가능한 ID입니다.");}
			 		} else {JOptionPane.showMessageDialog(memJoin, "ID는 대소문자와 숫자만 사용 가능합니다.");}
				}else {JOptionPane.showMessageDialog(memJoin, "ID에 공백을 사용할 수 없습니다.");}
			}else {JOptionPane.showMessageDialog(memJoin, "ID를 입력해주세요.");}
		}
		
		//회원 가입 버튼 클릭
		if(e.getSource().equals(memJoin.btnJoin)) {
			String id = memJoin.tfJoinId.getText();
			String pw = memJoin.pfJoinPw.getText();
			String name = memJoin.tfJoinName.getText();
			String email = memJoin.tfJoinEmail.getText();
			String birth = memJoin.tfJoinBirth.getText();
			if(!id.equals(checkText) && !pw.equals(checkText) && !name.equals(checkText)
					&& !email.equals(checkText) && !birth.equals(checkText)) { //공백인 칸이 없음
				//나이 유효성 검사 시작
				String ageStr = "^[0-9]{8}$";
				boolean ageTrue = Pattern.matches(ageStr, birth);
				if(ageTrue) { //생일이 숫자로만 기입돼있음
					String ageCut = birth.substring(0, 4);
					int ageCheck = Integer.parseInt(ageCut);
					Calendar current = Calendar.getInstance();
					int year = current.get(Calendar.YEAR);
					int age = year - ageCheck;
					if(age > 19) { //성인
						//회원가입 시 캐릭터 img 랜덤 생성
						int randomNum = (int) (Math.random()*7);
						String info_img ="src/mainImgs/userImg"+randomNum+".png";
						MemberDTO memDTO = new MemberDTO();
						memDTO.setId(id);
						memDTO.setPw(pw);
						memDTO.setName(name);
						memDTO.setEmail(email);
						memDTO.setBirth(birth);
						memDTO.setInfo_img(info_img);
						int insertMem = new MemberDAOImp().insertMember(memDTO);
						if(insertMem>0) { //가입 가능
							//10,000 코인 지급
							int firstJoin = new LogDAOImp().firstJoin(id);
							JOptionPane.showMessageDialog(memJoin, "가입을 축하드립니다(10,000코인 지급!)");
							memJoin.dispose();
						}
					} else { //미성년자
						JOptionPane.showMessageDialog(memJoin, "미성년자는 가입 할 수 없습니다. 게임을 종료합니다.");
						System.exit(0);
					}
				} else {JOptionPane.showMessageDialog(memJoin, "생년월일은 숫자만 입력 가능합니다. ex)18880101");}
			}else if (id.equals(checkText)) {JOptionPane.showMessageDialog(memJoin, "아이디를 입력해주세요.");
			}else if (memJoin.joinFlag==0) {JOptionPane.showMessageDialog(memJoin, "√ 아이디 중복체크를 해주세요");
			}else if(pw.equals(checkText)) {JOptionPane.showMessageDialog(memJoin, "비밀번호를 입력해주세요.");
			}else if(name.equals(checkText)) {JOptionPane.showMessageDialog(memJoin, "이름(닉네임)을 입력해주세요.");
			}else if(email.equals(checkText)) {JOptionPane.showMessageDialog(memJoin, "이메일을 입력해주세요.");
			}else if(birth.equals(checkText)) {JOptionPane.showMessageDialog(memJoin, "생년월일을 입력해주세요.");
			}
		}
	}
}
