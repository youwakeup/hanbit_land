package com.hanbit_land.kjy.dto;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.hanbit_land.kjy.main.Main;

public class HBLandVO {
	
	//HBLand에 붙어있는 컴포넌트들
	
	//게임 실행 시 첫 화면
	public JButton btnLogin;
	public JButton btnJoin;
	public JTextField tfLoginId;
	public JPasswordField pfLoginPw;
	public JLabel menuBar;
	public JButton btnMenuExit; 
	
	//toMain
	public JButton btnToMain;
	public JButton btnExit;
	
	//memberInfo
	public JLabel lbInfoImg;
	public JLabel lbInfoId;
	public JLabel lbInfoName;
	public JLabel lbInfoEmail;
	public JLabel lbInfoBirth;
	public JLabel lbInfoRecord;
	public JLabel lbInfoCoin;
	public JButton btnInfoEdit;
	public JButton btnImgEdit;
	public JButton btnCoinFreeImg;
	
	//gameSelect
	public JButton btnSelectSlot;
	public JButton btnSelectPoker;
	public JButton btnSelectBlack;
	public JButton btnSelectSudda;
	
	//gamePanel(나중에 new 게임 패널(); 호출)
	public JPanel slotP;
	public JPanel pokerP;
	public JPanel blackP;
	public JPanel suddaP;
	public JButton getBtnLogin() {
		return btnLogin;
	}
	public void setBtnLogin(JButton btnLogin) {
		this.btnLogin = btnLogin;
	}
	public JButton getBtnJoin() {
		return btnJoin;
	}
	public void setBtnJoin(JButton btnJoin) {
		this.btnJoin = btnJoin;
	}
	public JTextField getTfLoginId() {
		return tfLoginId;
	}
	public void setTfLoginId(JTextField tfLoginId) {
		this.tfLoginId = tfLoginId;
	}
	public JPasswordField getPfLoginPw() {
		return pfLoginPw;
	}
	public void setPfLoginPw(JPasswordField pfLoginPw) {
		this.pfLoginPw = pfLoginPw;
	}
	public JLabel getMenuBar() {
		return menuBar;
	}
	public void setMenuBar(JLabel menuBar) {
		this.menuBar = menuBar;
	}
	public JButton getBtnMenuExit() {
		return btnMenuExit;
	}
	public void setBtnMenuExit(JButton btnMenuExit) {
		this.btnMenuExit = btnMenuExit;
	}
	public JButton getBtnToMain() {
		return btnToMain;
	}
	public void setBtnToMain(JButton btnToMain) {
		this.btnToMain = btnToMain;
	}
	public JButton getBtnExit() {
		return btnExit;
	}
	public void setBtnExit(JButton btnExit) {
		this.btnExit = btnExit;
	}
	public JLabel getLbInfoImg() {
		return lbInfoImg;
	}
	public void setLbInfoImg(JLabel lbInfoImg) {
		this.lbInfoImg = lbInfoImg;
	}
	public JLabel getLbInfoId() {
		return lbInfoId;
	}
	public void setLbInfoId(JLabel lbInfoId) {
		this.lbInfoId = lbInfoId;
	}
	public JLabel getLbInfoName() {
		return lbInfoName;
	}
	public void setLbInfoName(JLabel lbInfoName) {
		this.lbInfoName = lbInfoName;
	}
	public JLabel getLbInfoEmail() {
		return lbInfoEmail;
	}
	public void setLbInfoEmail(JLabel lbInfoEmail) {
		this.lbInfoEmail = lbInfoEmail;
	}
	public JLabel getLbInfoBirth() {
		return lbInfoBirth;
	}
	public void setLbInfoBirth(JLabel lbInfoBirth) {
		this.lbInfoBirth = lbInfoBirth;
	}
	public JLabel getLbInfoRecord() {
		return lbInfoRecord;
	}
	public void setLbInfoRecord(JLabel lbInfoRecord) {
		this.lbInfoRecord = lbInfoRecord;
	}
	public JLabel getLbInfoCoin() {
		return lbInfoCoin;
	}
	public void setLbInfoCoin(JLabel lbInfoCoin) {
		this.lbInfoCoin = lbInfoCoin;
	}
	public JButton getBtnInfoEdit() {
		return btnInfoEdit;
	}
	public void setBtnInfoEdit(JButton btnInfoEdit) {
		this.btnInfoEdit = btnInfoEdit;
	}
	public JButton getBtnImgEdit() {
		return btnImgEdit;
	}
	public void setBtnImgEdit(JButton btnImgEdit) {
		this.btnImgEdit = btnImgEdit;
	}
	public JButton getBtnCoinFreeImg() {
		return btnCoinFreeImg;
	}
	public void setBtnCoinFreeImg(JButton btnCoinFreeImg) {
		this.btnCoinFreeImg = btnCoinFreeImg;
	}
	public JButton getBtnSelectSlot() {
		return btnSelectSlot;
	}
	public void setBtnSelectSlot(JButton btnSelectSlot) {
		this.btnSelectSlot = btnSelectSlot;
	}
	public JButton getBtnSelectPoker() {
		return btnSelectPoker;
	}
	public void setBtnSelectPoker(JButton btnSelectPoker) {
		this.btnSelectPoker = btnSelectPoker;
	}
	public JButton getBtnSelectBlack() {
		return btnSelectBlack;
	}
	public void setBtnSelectBlack(JButton btnSelectBlack) {
		this.btnSelectBlack = btnSelectBlack;
	}
	public JButton getBtnSelectSudda() {
		return btnSelectSudda;
	}
	public void setBtnSelectSudda(JButton btnSelectSudda) {
		this.btnSelectSudda = btnSelectSudda;
	}
	public JPanel getSlotP() {
		return slotP;
	}
	public void setSlotP(JPanel slotP) {
		this.slotP = slotP;
	}
	public JPanel getPokerP() {
		return pokerP;
	}
	public void setPokerP(JPanel pokerP) {
		this.pokerP = pokerP;
	}
	public JPanel getBlackP() {
		return blackP;
	}
	public void setBlackP(JPanel blackP) {
		this.blackP = blackP;
	}
	public JPanel getSuddaP() {
		return suddaP;
	}
	public void setSuddaP(JPanel suddaP) {
		this.suddaP = suddaP;
	}
	
	@Override
	public String toString() {
		return "HBLandVO [btnLogin=" + btnLogin + ", btnJoin=" + btnJoin + ", tfLoginId=" + tfLoginId + ", pfLoginPw="
				+ pfLoginPw + ", menuBar=" + menuBar + ", btnMenuExit=" + btnMenuExit + ", btnToMain=" + btnToMain
				+ ", btnExit=" + btnExit + ", lbInfoImg=" + lbInfoImg + ", lbInfoId=" + lbInfoId + ", lbInfoName="
				+ lbInfoName + ", lbInfoEmail=" + lbInfoEmail + ", lbInfoBirth=" + lbInfoBirth + ", lbInfoRecord="
				+ lbInfoRecord + ", lbInfoCoin=" + lbInfoCoin + ", btnInfoEdit=" + btnInfoEdit + ", btnImgEdit="
				+ btnImgEdit + ", btnCoinFreeImg=" + btnCoinFreeImg + ", btnSelectSlot=" + btnSelectSlot
				+ ", btnSelectPoker=" + btnSelectPoker + ", btnSelectBlack=" + btnSelectBlack + ", btnSelectSudda="
				+ btnSelectSudda + ", slotP=" + slotP + ", pokerP=" + pokerP + ", blackP=" + blackP + ", suddaP="
				+ suddaP + "]";
	}
}
