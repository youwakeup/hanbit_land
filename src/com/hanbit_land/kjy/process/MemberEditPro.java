package com.hanbit_land.kjy.process;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.view.MemberEdit;

public class MemberEditPro implements ActionListener {

	MemberEdit memEdit;
	HBLandVO hbVO;
	HBLand hbLand;

	public MemberEditPro(MemberEdit memEdit) {
		this.memEdit = memEdit;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		//회원 정보 수정 버튼
		if (e.getSource().equals(memEdit.btnEdit)) {
			String pw = memEdit.pfPwEdit.getText();
			String name = memEdit.tfNameEdit.getText();
			String email = memEdit.tfEmailEdit.getText();
			hbLand.memDTO.setPw(pw);
			hbLand.memDTO.setName(name);
			hbLand.memDTO.setEmail(email);
			int update = new MemberDAOImp().updateMember(hbLand.memDTO);
			if (update > 0) {
				JOptionPane.showMessageDialog(memEdit, hbLand.memDTO.getId() + "님의 정보 수정이 완료되었습니다.");
				memEdit.dispose();
				JLabel lbInfoName = hbLand.hbVO.getLbInfoName();
				JLabel lbInfoEmail = hbLand.hbVO.getLbInfoEmail();
				//라벨 다시 그리기
				lbInfoName.setText(name);
				lbInfoEmail.setText(email);
				lbInfoName.repaint();
				lbInfoName.revalidate();
				lbInfoEmail.repaint();
				lbInfoEmail.revalidate();
				hbLand.hbVO.setLbInfoName(lbInfoName);
				hbLand.hbVO.setLbInfoEmail(lbInfoEmail);
			} else if(update < 0){
				JOptionPane.showMessageDialog(memEdit, "비밀번호를 입력해주세요.");
			}
		}
	}
}
