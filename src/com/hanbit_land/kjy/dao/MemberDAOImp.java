package com.hanbit_land.kjy.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.util.OracleDBManager;

public class MemberDAOImp implements MemberDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	//id 중복 체크, 회원 정보 출력
	@Override
	public MemberDTO selectMember(String id) {
		MemberDTO memDTO = new MemberDTO();
		String sql = "SELECT * FROM member WHERE id=?";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				memDTO.setNum(rs.getInt("num"));
				memDTO.setId(rs.getString("id"));
				memDTO.setPw(rs.getString("pw"));
				memDTO.setName(rs.getString("name"));
				memDTO.setEmail(rs.getString("email"));
				memDTO.setBirth(rs.getString("birth"));
				memDTO.setInfo_img(rs.getString("info_img"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(rs, pstmt, conn);
		}
		return memDTO;
	}

	//로그인
	@Override
	public MemberDTO selectMember(String id, String pw) {
		MemberDTO memDTO = null;
		String sql = "SELECT * FROM member WHERE id=? AND pw=?";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, pw);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				memDTO = new MemberDTO();
				memDTO.setNum(rs.getInt("num"));
				memDTO.setId(rs.getString("id"));
				memDTO.setPw(rs.getString("pw"));
				memDTO.setName(rs.getString("name"));
				memDTO.setEmail(rs.getString("email"));
				memDTO.setBirth(rs.getString("birth"));
				memDTO.setInfo_img(rs.getString("info_img"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(rs, pstmt, conn);
		}
		return memDTO;
	}

	//회원 정보 수정
	@Override
	public int updateMember(MemberDTO memDTO) {
		int updateMem = 0;
		String sql = "UPDATE member SET pw=?, name=?, email=? WHERE num=(SELECT num FROM member WHERE id=?)";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memDTO.getPw());
			pstmt.setString(2, memDTO.getName());
			pstmt.setString(3, memDTO.getEmail());
			pstmt.setString(4, memDTO.getId());
			updateMem = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(pstmt, conn);
		}
		return updateMem;
	}

	//회원 가입
	@Override
	public int insertMember(MemberDTO memDTO) {
		int insertMem = 0;
		String sql = "INSERT INTO member VALUES (member_seq.nextval, ?, ?, ?, ?, ?, ?)";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memDTO.getId());
			pstmt.setString(2, memDTO.getPw());
			pstmt.setString(3, memDTO.getName());
			pstmt.setString(4, memDTO.getEmail());
			pstmt.setString(5, memDTO.getBirth());
			pstmt.setString(6, memDTO.getInfo_img());
			insertMem = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(pstmt, conn);
		}
		return insertMem;
	}

	//회원 탈퇴
	@Override
	public int deleteMember(int num) {
		int deleteMem = 0;
		String sql = "DELETE FROM member WHERE num=?";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			deleteMem = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(pstmt, conn);
		}
		return deleteMem;
	}

	//캐릭터 이미지 수정
	@Override
	public int updateInfoImg(String info_img, String id) {
		int infoImg = 0;
		String sql = "UPDATE member SET info_img=? WHERE num=(SELECT num FROM member WHERE id=?)";
		try {
			conn = OracleDBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, info_img);
			pstmt.setString(2, id);
			infoImg = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OracleDBManager.close(pstmt, conn);
		}
		return infoImg;
	}
}
