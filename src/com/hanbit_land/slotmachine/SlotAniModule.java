package com.hanbit_land.slotmachine;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;


public class SlotAniModule extends JPanel{
	private List<ScrollAnimator> list=new ArrayList<ScrollAnimator>();
	private Callback back;
	SlotGame rule;
	public SlotAniModule(int x,int y,int width,int height,Callback back){
		setBounds(x, y, width, height);
		this.back=back;
		int slotX=(width/3/32)*32;
		setLayout(null);
		
		ScrollAnimator slot1 = new ScrollAnimator(back);
		slot1.setBounds(0, 0, slotX,slotX*2);
		add(slot1);
		
		ScrollAnimator slot2 = new ScrollAnimator(back);
		slot2.setBounds(slotX, 0,slotX,slotX*2);
		add(slot2);
		
		ScrollAnimator slot3 = new ScrollAnimator(back);
		slot3.setBounds(slotX*2, 0,slotX,slotX*2);
		add(slot3);
		
		list.add(slot1);
		list.add(slot2);
		list.add(slot3);
	}
	/**슬롯 돌리기*/
	public void start() {
		for (ScrollAnimator slot : list) {
			slot.start();
		}
	}
	/**숫자 1~3까지만 입력*/
	public void stop(int num) {
		num=num-1;
		list.get(num).stop();
	}
}
