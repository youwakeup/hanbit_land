package com.hanbit_land.slotmachine;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.main.HBLand;
//import com.sun.scenario.effect.AbstractShadow.ShadowMode;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.event.MouseInputAdapter;


public class SlotMachine extends JPanel{
	private final int gameCK=0;//슬롯머신로그=0번
	private final int bat=100;//배팅비
	private final int slotY=80;//슬롯버튼 위치
	private boolean isReady=false;
	
	private HBLand land;
	private int count=0;
	private int val1=0;
	private int val2=0;
	private int val3=0;
	private int posY;
	private int localY;
	private int y;
	
	private SlotAniModule module;
	private SlotGame rule;
	private JButton batBtn;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JLabel infoLabel;
	private JButton slotBtn;
	private Callback call;
	
	public SlotMachine(HBLand land){
		////
		this.land=land;
		////
		setBounds(0, 0, 480, 640);
		setBackground(Color.BLACK);
		setLayout(null);
		
		//결과 판단
		call=new Callback() {			
			@Override
			public void call(int value) {
				count++;
				gameResult(value);
			}
		};
		//슬롯 모듈
		module=new SlotAniModule(90,80,288,192,call);
		add(module);
		//게임 룰
		rule=new SlotGame(0.9f, bat);
		//배팅버튼
		batBtn=new JButton("BAT");
		batBtn.addActionListener((e)->{readyToStart();});
		batBtn.setBounds(408, 334, 60, 60);
		add(batBtn);
		//슬롯1멈춤버튼
		btn1 = new JButton("");
		btn1.addActionListener((e)->{module.stop(1);});
		btn1.setBounds(56, 349, 45, 45);
		add(btn1);
		//슬롯2멈춤버튼
		btn2 = new JButton("");
		btn2.addActionListener((e)->{module.stop(2);});
		btn2.setBounds(165, 349, 45, 45);
		add(btn2);
		//슬롯3멈춤버튼
		btn3 = new JButton("");
		btn3.addActionListener((e)->{module.stop(3);});
		btn3.setBounds(274, 349, 45, 45);
		add(btn3);
		//전광판같은거
		infoLabel = new JLabel("SlotMachine");
		infoLabel.setForeground(Color.WHITE);
		infoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		infoLabel.setFont(new Font("Consolas", Font.PLAIN, 45));
		infoLabel.setBounds(0, 0, 480, 100);
		add(infoLabel);
		//슬롯 땡기기
		slotBtn=new JButton();
		
		slotBtn.addMouseListener(new MouseInputAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(isReady) {
					posY=e.getYOnScreen();
					localY=slotBtn.getLocation().y;
				}
			}
			@Override
			public void mouseReleased(MouseEvent arg0) {
				System.out.println("릴리즈");
				if(isReady) {
					autoUpStick();
					if(slotBtn.getLocation().y>250) {start();System.out.println("시작- ");}
				}
			}
		});
		slotBtn.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if(isReady) {
				int newLocalY=e.getYOnScreen()-posY+localY;					
				if(newLocalY>80 &&newLocalY<280) slotBtn.setLocation(415,newLocalY);
				}
			}
		});
		slotBtn.setBounds(417, 98, 40, 40);
		add(slotBtn);
		
		initGame();
	}
	private void initGame() {
		batBtn.setEnabled(true);
		btn1.setEnabled(false);
		btn2.setEnabled(false);
		btn3.setEnabled(false);
		slotBtn.setEnabled(false);
		//HBLand main=new HBLand();
		//if(land!=null)land.infoMem.lbCoin.setText("1111");
	}
	
	//슬롯 돌리기
	private void start() {
		isReady=false;
		btn1.setEnabled(true);
		btn2.setEnabled(true);
		btn3.setEnabled(true);
		slotBtn.setEnabled(false);
		module.start();
	}
	//슬롯 돌릴 준비
	private void readyToStart() {
		int preCoin=0;
		try {
			//preCoin=Integer.parseInt(land.infoMem.lbCoin.getText());			
			preCoin=Integer.parseInt(land.hbVO.lbInfoCoin.getText());			
			if(preCoin<bat) {
				JOptionPane.showMessageDialog(null,"코인이 모자랍니다");
			}else {
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"널널널");
			LogDTO dto=new LogDAOImp().selectCoin(land.memDTO.getId());
			preCoin=dto.getCoin();
			System.out.println("====================================121231============");
			System.out.println(dto.getCoin());
			//land.infoMem.lbCoin.setText(Integer.toString(preCoin));
			land.hbVO.lbInfoCoin.setText(Integer.toString(preCoin));
		}finally {
			
			isReady=true;
			batBtn.setEnabled(false);
			infoLabel.setText("Get Ready");
			setInfoP(-bat);
			//land.gameSelect.btnToMain.setVisible(false);
			land.hbVO.btnToMain.setVisible(false);
			slotBtn.setEnabled(true);
		}
		
	}
	private void autoUpStick() {
		System.out.println("thread");
		Thread th=new Thread(()->{
			y=slotBtn.getLocation().y;
			do {
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				y+=-5;
				slotBtn.setLocation(415, y);
			} while(y>slotY);
			System.out.println("threadssssssssssss");
		});
		th.start();
	}
	private void gameResult(int value) {
		if(count==1) {
			val1=value;
		}else if(count==2) {
			val2=value;
		}if(count==3) {
			val3=value;
			count=0;
			btn1.setEnabled(false);
			btn2.setEnabled(false);
			btn3.setEnabled(false);
			batBtn.setEnabled(true);
			int result=rule.result(val1, val2, val3);
			if(result==0) {//잃었을때
				setInfoP(result, 1);
				setLog(1,-bat);
				infoLabel.setText("You Lost");
			}else {//땄을때
				setInfoP(result, 0);
				setLog(0,result);
				infoLabel.setText("Congratulation");
			}
			//land.gameSelect.btnToMain.setVisible(true);
			land.hbVO.btnToMain.setVisible(true);
			System.out.println("결과="+result);
		}
	}
	private void setInfoP(int coin) {
		int preCoin=0;
		try {
			//preCoin=Integer.parseInt(land.infoMem.lbCoin.getText());			
			preCoin=Integer.parseInt(land.hbVO.lbInfoCoin.getText());				
		} catch (Exception e) {
			
		}
		preCoin+=coin;
		//land.infoMem.lbCoin.setText(Integer.toString(preCoin));
		land.hbVO.lbInfoCoin.setText(Integer.toString(preCoin));
	}
	
	private void setInfoP(int coin, int result) {
		int preCoin=0;
		int win=new LogDAOImp().selectResult(0, HBLand.memDTO.getId());
		int lose=new LogDAOImp().selectResult(1, HBLand.memDTO.getId());
		if(result==0) {win++;}else lose++;
		String re = Integer.toString(win)+"/"+Integer.toString(win+lose);
		try {
			//preCoin=Integer.parseInt(land.infoMem.lbCoin.getText());			
			preCoin=Integer.parseInt(land.hbVO.lbInfoCoin.getText());			
		} catch (Exception e) {
			
		}
		preCoin+=coin;
		//land.infoMem.lbRecord.setText(re);
		//land.infoMem.lbCoin.setText(Integer.toString(preCoin));
		land.hbVO.lbInfoRecord.setText(re);
		land.hbVO.lbInfoCoin.setText(Integer.toString(preCoin));
	}
	/***result 0:승 1:패*/
	private void setLog(int result,int coin) {
		int id=land.memDTO.getNum();
		land.logDTO.setMem_num(id);
		land.logDTO.setGame(gameCK);
		land.logDTO.setCoin(coin);
		land.logDTO.setResult(result);
		new LogDAOImp().insertLog(land.logDTO);
	}
}
