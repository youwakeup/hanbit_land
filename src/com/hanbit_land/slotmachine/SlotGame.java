package com.hanbit_land.slotmachine;

public class SlotGame {
	/* 같은 그림일때만 승
	 * 그림 7개 *3이니까 경우의 수 7/(7*7*7)=7/343=1/49
	 * 각 배당=49 배당총합=49*7=343
	 * */
	private int bat;//배팅금액 또는 칩
	private float divRate;//배당률 1일때 잃은돈=딴돈
	private int[] slotRatio={2,3,4,5,6,7,22};//체리,사과,포도,오렌지,하트,세븐
	private float ratio=0f;
	private int sum=0;
	private final int ratioSum=343;//배당총합
	
	
	/**배당율 0.8~1정도가 적당, 배팅은 일단 10정도로*/
	public SlotGame(float divRate,int bat) {
		this.bat=bat;
		this.divRate=divRate;
		for(int i=0;i<slotRatio.length;i++) {
			sum+=slotRatio[i];
		}
		ratio=ratioSum/sum;
	}
	
	public int result(int slot1,int slot2, int slot3) {
		int result=0;
		if(slot1==slot2&&slot1==slot3) {
			result=(int)(slotRatio[slot1]*bat*ratio*divRate);
		}
		return result;
	}
	public int[] payTable() {
		int[] table=new int[7];
		for(int i=0;i<slotRatio.length;i++) {
			table[i]=(int)(slotRatio[i]*bat*ratio*divRate);
		}
		return table;
	}
}
