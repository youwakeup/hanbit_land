package com.hanbit_land.kjy.main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.dto.MemberDTO;
import com.hanbit_land.kjy.process.MemberLoginPro;
import com.hanbit_land.kjy.view.MemberJoin;

public class HBLand extends JFrame{

	//static DTO
	public static MemberDTO memDTO = new MemberDTO();
	public static LogDTO logDTO = new LogDTO();
	public static HBLandVO hbVO = new HBLandVO();
	
	//HBLand(private 하면 안됨)
	public Image mainImg;
	public Graphics mainGraphic;
	public Image backgroundImg = new ImageIcon("src/mainImgs/mainImg.png").getImage();
	public Image gameSelectImg = new ImageIcon("src/mainImgs/lobbyImg.png").getImage();
	public int mouseX, mouseY;
	public boolean isLobbyImg = false;
	
	//생성자
	public HBLand() {
		setUndecorated(true);
		setSize(Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setBackground(new Color(0, 0, 0, 0));
		setLayout(null);
		
//상단 바 종료 버튼
		JButton btnMenuExit = new JButton(new ImageIcon("src/mainImgs/menuExitImg.png"));
		btnMenuExit.setBounds(750, 0, 30, 30);
		btnMenuExit.setBorderPainted(false);
		btnMenuExit.setContentAreaFilled(false);
		btnMenuExit.setFocusPainted(false);
		btnMenuExit.addMouseListener(new hbInnerClass());
		hbVO.setBtnMenuExit(btnMenuExit);
		add(hbVO.getBtnMenuExit());
		
//상단 바 삽입
		JLabel menuBar = new JLabel(new ImageIcon("src/mainImgs/menuBarImg.png"));
		menuBar.setBounds(0, 0, 780, 30);
		menuBar.addMouseListener(new mousePoint());
		menuBar.addMouseMotionListener(new mouseMotionPoint());
		hbVO.setMenuBar(menuBar);
		add(hbVO.getMenuBar());
		
//회원가입 버튼
		JButton btnJoin = new JButton(new ImageIcon("src/mainImgs/btnJoinImg.png"));
		btnJoin.setBounds(320, 615, 110, 34);
		btnJoin.setBorderPainted(false);
		btnJoin.setContentAreaFilled(false);
		btnJoin.setFocusPainted(false);
		btnJoin.addMouseListener(new hbInnerClass());
		hbVO.setBtnJoin(btnJoin);
		add(hbVO.getBtnJoin());
		
//id & pw 입력 필드
		JTextField tfLoginId = new JTextField();
		tfLoginId.setBounds(350, 550, 150, 30);
		hbVO.setTfLoginId(tfLoginId);
		add(hbVO.getTfLoginId());
		
		JPasswordField pfLoginPw = new JPasswordField();
		pfLoginPw.setBounds(350, 585, 150, 30);
		hbVO.setPfLoginPw(pfLoginPw);
		add(hbVO.getPfLoginPw());
		
//로그인 버튼
		JButton btnLogin = new JButton(new ImageIcon("src/mainImgs/btnLoginImg.png"));
		btnLogin.setBounds(410, 615, 110, 34);
		btnLogin.setBorderPainted(false);
		btnLogin.setContentAreaFilled(false);
		btnLogin.setFocusPainted(false);
		btnLogin.addMouseListener(new MemberLoginPro(HBLand.this));
		hbVO.setBtnLogin(btnLogin);
		add(hbVO.getBtnLogin());
		
	}	
	
	//게임 화면 그리기
	public void paint(Graphics g) {
		mainImg = createImage(Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT);
		mainGraphic = mainImg.getGraphics();
		mainDraw(mainGraphic);
		g.drawImage(mainImg, 0, 0, null);
	}
	public void mainDraw(Graphics g) {
		g.drawImage(backgroundImg, 0, 30, null);
		if(isLobbyImg) g.drawImage(gameSelectImg, 0, 30, null);
		paintComponents(g);
		this.repaint();
	}
	
	//HB Inner Class
	class hbInnerClass extends MouseAdapter {
		@Override
		public void mouseEntered(MouseEvent e) {
			hbVO.btnMenuExit.setCursor(new Cursor(Cursor.HAND_CURSOR));
			hbVO.btnJoin.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		@Override
		public void mouseExited(MouseEvent e) {
			hbVO.btnMenuExit.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			hbVO.btnJoin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
		@Override
		public void mousePressed(MouseEvent e) {
			
			//상단 바  EXIT 버튼
			if(e.getSource().equals(hbVO.getBtnMenuExit())) {
				int yes = JOptionPane.showConfirmDialog(hbVO.getBtnMenuExit(), "게임을 종료하시겠습니까?", "HANBIT LAND", JOptionPane.YES_NO_OPTION);
				if(yes == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
			
			//회원 가입 버튼
			if(e.getSource().equals(hbVO.getBtnJoin())) {
				new MemberJoin();
			}
		}
	}
	
	//상단 바 이동 이벤트(Inner Class)
	class mousePoint extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			mouseX = e.getX();
			mouseY = e.getY();
		}
	}
	class mouseMotionPoint extends MouseMotionAdapter {
		@Override
		public void mouseDragged(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			setLocation(x - mouseX, y - mouseY);
		}
	}
}
