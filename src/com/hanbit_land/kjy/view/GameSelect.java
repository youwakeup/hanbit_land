package com.hanbit_land.kjy.view;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.hanbit_land.bj.BjGui;
import com.hanbit_land.jds.PokerGUI;
import com.hanbit_land.kdh.SuddaGui;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.main.Main;
import com.hanbit_land.slotmachine.SlotMachine;

public class GameSelect {
	
	HBLand hbLand;
	int gameStatus = 0;
	
	public GameSelect(HBLand hbLand) {
		this.hbLand = hbLand;
		
		//게임 -> 로비
		JButton btnToMain = new JButton(new ImageIcon("src/mainImgs//btnMainImg.png"));
		btnToMain.setBounds(600, 590, 80, 70);
		btnToMain.setBorderPainted(false);
		btnToMain.setContentAreaFilled(false);
		btnToMain.setFocusPainted(false);
		btnToMain.setVisible(false);
		btnToMain.addMouseListener(new MouseAdapter() {
			@Override public void mouseEntered(MouseEvent e) {btnToMain.setCursor(new Cursor(Cursor.HAND_CURSOR));}
			@Override public void mouseExited(MouseEvent e) {btnToMain.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));}
			@Override public void mousePressed(MouseEvent e) {
				if(gameStatus==1) { //게임 패널이 hbLand 위에 덮어졌을 때 1==슬롯 / 2==블랙잭 / 3==포커 / 4==섯다
					hbLand.remove(HBLand.hbVO.slotP);
					gameOut();
					HBLand.hbVO.setBtnToMain(btnToMain);
				}else if(gameStatus==2) {
					hbLand.remove(HBLand.hbVO.blackP);
					gameOut();
					HBLand.hbVO.setBtnToMain(btnToMain);
				}else if(gameStatus==3) {
					hbLand.remove(HBLand.hbVO.pokerP);
					gameOut();
					HBLand.hbVO.setBtnToMain(btnToMain);
				}else if(gameStatus==4) {
					hbLand.remove(HBLand.hbVO.suddaP);
					gameOut();
					HBLand.hbVO.setBtnToMain(btnToMain);
				}
			}});
		HBLand.hbVO.setBtnToMain(btnToMain);
		hbLand.add(HBLand.hbVO.getBtnToMain());
		
		//슬롯
		JButton btnSelectSlot = new JButton(new ImageIcon("src/mainImgs/slotTextImg.png"));
		btnSelectSlot.setBounds(40, 180, 200, 140);
		btnSelectSlot.setBorderPainted(false);
		btnSelectSlot.setContentAreaFilled(false);
		btnSelectSlot.setFocusPainted(false);
		btnSelectSlot.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btnSelectSlot.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				btnSelectSlot.setIcon(new ImageIcon("src/mainImgs/slotTextImg.png"));
				hbLand.isLobbyImg = false;
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSelectSlot.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnSelectSlot.setIcon(new ImageIcon("src/mainImgs/slotTextBig.png"));
				hbLand.isLobbyImg = true;
				hbLand.gameSelectImg = new ImageIcon("src/mainImgs/selectSlotImg.png").getImage();
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				HBLand.hbVO.slotP = new SlotMachine(hbLand);
				HBLand.hbVO.slotP.setBounds(0, 30, 480, 640);
				HBLand.hbVO.slotP.setVisible(true);
				hbLand.add(HBLand.hbVO.slotP);
				gameIn();
				gameStatus = 1;
			}
		});
		HBLand.hbVO.setBtnSelectSlot(btnSelectSlot);
		hbLand.add(HBLand.hbVO.getBtnSelectSlot());
		
		//인디언 포커
		JButton btnSelectPoker = new JButton(new ImageIcon("src/mainImgs/pokerTextImg.png"));
		btnSelectPoker.setBounds(40, 350, 200, 140);
		btnSelectPoker.setBorderPainted(false);
		btnSelectPoker.setContentAreaFilled(false);
		btnSelectPoker.setFocusPainted(false);
		btnSelectPoker.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btnSelectPoker.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				btnSelectPoker.setIcon(new ImageIcon("src/mainImgs/pokerTextImg.png"));
				hbLand.isLobbyImg = false;
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSelectPoker.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnSelectPoker.setIcon(new ImageIcon("src/mainImgs/pokerTextBig.png"));
				hbLand.isLobbyImg = true;
				hbLand.gameSelectImg = new ImageIcon("src/mainImgs/selectPokerImg.png").getImage();
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				HBLand.hbVO.pokerP = new PokerGUI(hbLand);
				HBLand.hbVO.pokerP.setBounds(0, 30, 480, 640);
				HBLand.hbVO.pokerP.setVisible(true);
				hbLand.add(HBLand.hbVO.pokerP);
				gameIn();
				gameStatus = 3;
			}
		});
		HBLand.hbVO.setBtnSelectPoker(btnSelectPoker);
		hbLand.add(HBLand.hbVO.getBtnSelectPoker());
		
		//블랙잭
		JButton btnSelectBlack = new JButton(new ImageIcon("src/mainImgs/blackTextImg.png"));
		btnSelectBlack.setBounds(240, 160, 200, 140);
		btnSelectBlack.setBorderPainted(false);
		btnSelectBlack.setContentAreaFilled(false);
		btnSelectBlack.setFocusPainted(false);
		btnSelectBlack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btnSelectBlack.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				btnSelectBlack.setIcon(new ImageIcon("src/mainImgs/blackTextImg.png"));
				hbLand.isLobbyImg = false;
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSelectBlack.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnSelectBlack.setIcon(new ImageIcon("src/mainImgs/blackTextBig.png"));
				hbLand.isLobbyImg = true;
				hbLand.gameSelectImg = new ImageIcon("src/mainImgs/selectBlackImg.png").getImage();
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				HBLand.hbVO.blackP = new BjGui();
				HBLand.hbVO.blackP.setBounds(0, 30, 480, 640);
				HBLand.hbVO.blackP.setVisible(true);
				hbLand.add(HBLand.hbVO.blackP);
				gameIn();
				gameStatus = 2;
			}
		});
		HBLand.hbVO.setBtnSelectBlack(btnSelectBlack);
		hbLand.add(HBLand.hbVO.getBtnSelectBlack());
		
		//섯다
		JButton btnSelectSudda = new JButton(new ImageIcon("src/mainImgs/suddaTextImg.png"));
		btnSelectSudda.setBounds(240, 350, 200, 140);
		btnSelectSudda.setBorderPainted(false);
		btnSelectSudda.setContentAreaFilled(false);
		btnSelectSudda.setFocusPainted(false);
		btnSelectSudda.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btnSelectSudda.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				btnSelectSudda.setIcon(new ImageIcon("src/mainImgs/suddaTextImg.png"));
				hbLand.isLobbyImg = false;
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSelectSudda.setCursor(new Cursor(Cursor.HAND_CURSOR));
				btnSelectSudda.setIcon(new ImageIcon("src/mainImgs/suddaTextBig.png"));
				hbLand.isLobbyImg = true;
				hbLand.gameSelectImg = new ImageIcon("src/mainImgs/selectSuddaImg.png").getImage();
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				HBLand.hbVO.suddaP = new SuddaGui(hbLand);
				HBLand.hbVO.suddaP.setBounds(0, 30, 480, 640);
				HBLand.hbVO.suddaP.setVisible(true);
				hbLand.add(HBLand.hbVO.suddaP);
				gameIn();
				gameStatus = 4;
			}
		});
		HBLand.hbVO.setBtnSelectSudda(btnSelectSudda);
		hbLand.add(HBLand.hbVO.getBtnSelectSudda());
		
	}
	
	
	public void gameOut() {
		HBLand.hbVO.btnToMain.setVisible(false);
		HBLand.hbVO.btnSelectSlot.setVisible(true);
		HBLand.hbVO.btnSelectPoker.setVisible(true);
		HBLand.hbVO.btnSelectBlack.setVisible(true);
		HBLand.hbVO.btnSelectSudda.setVisible(true);
	}
	
	public void gameIn() {
		HBLand.hbVO.btnToMain.setVisible(true);
		HBLand.hbVO.btnSelectSlot.setVisible(false);
		HBLand.hbVO.btnSelectPoker.setVisible(false);
		HBLand.hbVO.btnSelectBlack.setVisible(false);
		HBLand.hbVO.btnSelectSudda.setVisible(false);
	}
	

}
