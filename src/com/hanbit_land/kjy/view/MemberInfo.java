package com.hanbit_land.kjy.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.main.Main;
import com.hanbit_land.kjy.process.MemberLoginPro;

public class MemberInfo {
	
	MemberLoginPro loginPro;
	public HBLand hbLand;
	HBLandVO hbVO;
	
	
	public MemberInfo(HBLand hbLand) {
		this.hbLand = hbLand;
		JLabel lbInfoImg = new JLabel();
		lbInfoImg.setIcon(new ImageIcon(HBLand.memDTO.getInfo_img()));
		lbInfoImg.setForeground(Color.BLACK);
		lbInfoImg.setFont(new Font("돋움", Font.BOLD, 14));
		lbInfoImg.setBounds(528, 115, 200, 200);
		HBLand.hbVO.setLbInfoImg(lbInfoImg);
		hbLand.add(HBLand.hbVO.getLbInfoImg());

		//id 출력 라벨
		JLabel lbInfoId = new JLabel();
		lbInfoId.setText(hbLand.memDTO.getId());
		lbInfoId.setForeground(Color.WHITE);
		lbInfoId.setBounds(620, 368, 130, 30);
		lbInfoId.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoId.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoId(lbInfoId);
		hbLand.add(HBLand.hbVO.getLbInfoId());
		
		//name 출력 라벨
		JLabel lbInfoName = new JLabel();
		lbInfoName.setText(HBLand.memDTO.getName());
		lbInfoName.setForeground(Color.WHITE);
		lbInfoName.setBounds(620, 398, 130, 30);
		lbInfoName.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoName.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoName(lbInfoName);
		hbLand.add(HBLand.hbVO.getLbInfoName());
		
		//email 출력 라벨
		JLabel lbInfoEmail = new JLabel();
		lbInfoEmail.setText(HBLand.memDTO.getEmail());
		lbInfoEmail.setForeground(Color.WHITE);
		lbInfoEmail.setBounds(620, 430, 130, 30);
		lbInfoEmail.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoEmail.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoEmail(lbInfoEmail);
		hbLand.add(HBLand.hbVO.getLbInfoEmail());
		
		//birth 출력 라벨
		JLabel lbInfoBirth = new JLabel();
		lbInfoBirth.setText(HBLand.memDTO.getBirth());
		lbInfoBirth.setForeground(Color.WHITE);
		lbInfoBirth.setBounds(620, 463, 130, 30);
		lbInfoBirth.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoBirth.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoBirth(lbInfoBirth);
		hbLand.add(HBLand.hbVO.getLbInfoBirth());
		
		//record 출력 라벨
		JLabel lbInfoRecord = new JLabel();
		lbInfoRecord.setForeground(Color.WHITE);
		lbInfoRecord.setBounds(620, 498, 130, 30);
		lbInfoRecord.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoRecord.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoRecord(lbInfoRecord);
		hbLand.add(HBLand.hbVO.getLbInfoRecord());
		
		//coin 출력 라벨
		JLabel lbInfoCoin = new JLabel();
		lbInfoCoin.setForeground(Color.WHITE);
		lbInfoCoin.setBounds(620, 530, 130, 30);
		lbInfoCoin.setHorizontalAlignment(SwingConstants.LEFT);
		lbInfoCoin.setFont(new Font("돋움", Font.PLAIN, 16));
		HBLand.hbVO.setLbInfoCoin(lbInfoCoin);
		hbLand.add(HBLand.hbVO.getLbInfoCoin());
		
		//회원 정보 수정 버튼
		JButton btnInfoEdit = new JButton(new ImageIcon("src/mainImgs/btnMemUpdateImg.png"));
		btnInfoEdit.setBounds(688, 320, 40, 40);
		btnInfoEdit.setBorderPainted(false);
		btnInfoEdit.setContentAreaFilled(false);
		btnInfoEdit.setFocusPainted(false);
		btnInfoEdit.addMouseListener(new infoInnerClass());
		HBLand.hbVO.setBtnInfoEdit(btnInfoEdit);
		hbLand.add(HBLand.hbVO.getBtnInfoEdit());
		
		//캐릭터 이미지 변경 버튼
		JButton btnImgEdit = new JButton(new ImageIcon("src/mainImgs/btnUserImg.png"));
		btnImgEdit.setBounds(640, 320, 40, 40);
		btnImgEdit.setBorderPainted(false);
		btnImgEdit.setContentAreaFilled(false);
		btnImgEdit.setFocusPainted(false);
		btnImgEdit.addMouseListener(new infoInnerClass());
		HBLand.hbVO.setBtnImgEdit(btnImgEdit);
		hbLand.add(HBLand.hbVO.getBtnImgEdit());
		
		//코인 무료 충전
		JButton btnCoinFreeImg = new JButton(new ImageIcon("src/mainImgs/coinFreeImg.png"));
		btnCoinFreeImg.setBounds(590, 320, 40, 40);
		btnCoinFreeImg.setBorderPainted(false);
		btnCoinFreeImg.setContentAreaFilled(false);
		btnCoinFreeImg.setFocusPainted(false);
		btnCoinFreeImg.addMouseListener(new infoInnerClass());
		HBLand.hbVO.setBtnCoinFreeImg(btnCoinFreeImg);
		hbLand.add(HBLand.hbVO.getBtnCoinFreeImg());
		
		//게임 종료 버튼
		JButton btnExit = new JButton(new ImageIcon("src/mainImgs/btnExitImg.png"));
		btnExit.setBounds(690, 590, 80, 70);
		btnExit.setBorderPainted(false);
		btnExit.setContentAreaFilled(false);
		btnExit.setFocusPainted(false);
		btnExit.addMouseListener(new infoInnerClass());
		btnExit.setVisible(true);
		HBLand.hbVO.setBtnExit(btnExit);
		hbLand.add(HBLand.hbVO.getBtnExit());
	}
	
	//회원 정보 InnerClass
	class infoInnerClass extends MouseAdapter{
		@Override
		public void mouseEntered(MouseEvent e) {
			HBLand.hbVO.btnImgEdit.setCursor(new Cursor(Cursor.HAND_CURSOR));
			HBLand.hbVO.btnInfoEdit.setCursor(new Cursor(Cursor.HAND_CURSOR));
			HBLand.hbVO.btnExit.setCursor(new Cursor(Cursor.HAND_CURSOR));
			HBLand.hbVO.btnCoinFreeImg.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
		
		@Override
		public void mouseExited(MouseEvent e) {
			HBLand.hbVO.btnImgEdit.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			HBLand.hbVO.btnInfoEdit.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			HBLand.hbVO.btnExit.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			HBLand.hbVO.btnCoinFreeImg.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			//캐릭터 이미지 수정 버튼
			if(e.getSource().equals(HBLand.hbVO.btnImgEdit)) { new MemberEditImg(MemberInfo.this);}
			//회원 정보 수정 버튼
			if(e.getSource().equals(HBLand.hbVO.btnInfoEdit)) { new MemberEdit(MemberInfo.this);}
			//게임 하단 EXIT 버튼
			if(e.getSource().equals(HBLand.hbVO.btnExit)) {
				int yes = JOptionPane.showConfirmDialog(HBLand.hbVO.btnExit, "게임을 종료하시겠습니까?", "HANBIT LAND", JOptionPane.YES_NO_OPTION);
				if(yes == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
			//코인 무료 충전 버튼
			if(e.getSource().equals(HBLand.hbVO.btnCoinFreeImg)) {
				//현재 코인 검사
				LogDTO logDTO = new LogDAOImp().selectCoin(HBLand.memDTO.getId());
				System.out.println("logDTO : "+logDTO);
				int nowCoin = logDTO.getCoin();
				if(nowCoin <= 0) { //현재 코인이 0 이거나 -n인 경우
					//무료 충전 횟수 검사
					int freeCoinCount = new LogDAOImp().selectFreeCoin(HBLand.memDTO.getId());
					if(freeCoinCount<4) { //'오늘' 충전 횟수가 3회를 넘지 않았을 경우
						coinCharge();
					}else {JOptionPane.showMessageDialog(hbLand, "무료 코인 충전 기회를 다 사용하였습니다.");}
				}else {JOptionPane.showMessageDialog(hbLand, "무료 코인 충전은 0 코인일 때 사용 가능합니다.");}
			}
		}
	}
	
	//무료 코인 충전 DAO
	public void coinCharge() {
		HBLand.logDTO.setMem_num(HBLand.memDTO.getNum());
		HBLand.logDTO.setGame(4);
		HBLand.logDTO.setResult(2);
		HBLand.logDTO.setCoin(+5000);
		int freeCoin = new LogDAOImp().insertLog(HBLand.logDTO);
		if(freeCoin>0) {
			JOptionPane.showMessageDialog(hbLand, "5,000 코인이 충전되었습니다.");
			HBLand.logDTO = new LogDAOImp().selectCoin(HBLand.memDTO.getId());
			HBLand.hbVO.lbInfoCoin.setText(Integer.toString(HBLand.logDTO.getCoin()));
		}
	}
}
