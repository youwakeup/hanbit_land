package com.hanbit_land.kjy.process;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.view.MemberEditImg;

public class MemberEditImgPro implements ActionListener{
	MemberEditImg imgEdit;
	
	public MemberEditImgPro(MemberEditImg imgEdit) {
		this.imgEdit = imgEdit;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(imgEdit.btnRadio1.isSelected()) {
			int imgNum=1;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg1.setBounds(528, 115, 200, 200);
			imgEdit.userImg1.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg1);
			defaultPro();
		}else if(imgEdit.btnRadio2.isSelected()) {
			int imgNum=2;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg2.setBounds(528, 115, 200, 200);
			imgEdit.userImg2.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg2);
			defaultPro();
		}else if(imgEdit.btnRadio3.isSelected()) {
			int imgNum=3;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg3.setBounds(528, 115, 200, 200);
			imgEdit.userImg3.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg3);
			defaultPro();
		}else if(imgEdit.btnRadio4.isSelected()) {
			int imgNum=4;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg4.setBounds(528, 115, 200, 200);
			imgEdit.userImg4.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg4);
			defaultPro();
		}else if(imgEdit.btnRadio5.isSelected()) {
			int imgNum=5;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg5.setBounds(528, 115, 200, 200);
			imgEdit.userImg5.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg5);
			defaultPro();
		}else if(imgEdit.btnRadio6.isSelected()) {
			int imgNum=6;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg6.setBounds(528, 115, 200, 200);
			imgEdit.userImg6.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg6);
			defaultPro();
		}else if(imgEdit.btnRadio7.isSelected()) {
			int imgNum=7;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg7.setBounds(528, 115, 200, 200);
			imgEdit.userImg7.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg7);
			defaultPro();
		}else if(imgEdit.btnRadio8.isSelected()) {
			int imgNum=8;
			String imgStr="src/mainImgs/userImg"+imgNum+".png";
			imgChange(imgStr);
			imgEdit.userImg8.setBounds(528, 115, 200, 200);
			imgEdit.userImg8.setBorder(null);
			imgEdit.memInfo.hbLand.add(imgEdit.userImg8);
			defaultPro();
		}
	}
	
	public void defaultPro() {
		HBLand.hbVO.lbInfoImg.setVisible(false);
		JOptionPane.showMessageDialog(imgEdit, "변경이 완료되었습니다.");
		imgEdit.dispose();
	}
	
	public void imgChange(String imgStr) {
		new MemberDAOImp().updateInfoImg(imgStr, HBLand.memDTO.getId());
	}
}
