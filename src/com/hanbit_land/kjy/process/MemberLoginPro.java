package com.hanbit_land.kjy.process;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.hanbit_land.kjy.dao.LogDAOImp;
import com.hanbit_land.kjy.dao.MemberDAOImp;
import com.hanbit_land.kjy.dto.HBLandVO;
import com.hanbit_land.kjy.dto.LogDTO;
import com.hanbit_land.kjy.main.HBLand;
import com.hanbit_land.kjy.main.Main;
import com.hanbit_land.kjy.view.GameSelect;
import com.hanbit_land.kjy.view.MemberInfo;

public class MemberLoginPro extends MouseAdapter{
	
	public HBLand hbLand;
	public HBLandVO hbVO;
	
	public MemberLoginPro(HBLand hbLand) {
		this.hbLand = hbLand;
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		HBLand.hbVO.btnLogin.setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
	@Override
	public void mouseExited(MouseEvent e) {
		HBLand.hbVO.btnLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
	@Override
	public void mousePressed(MouseEvent e) {
		String checkId="";
		if(e.getSource().equals(HBLand.hbVO.getBtnLogin())) { //로그인 버튼 클릭
			if(!HBLand.hbVO.getTfLoginId().getText().equals(checkId)) {//id 입력 필드 =/= null
				String id = HBLand.hbVO.getTfLoginId().getText();
				String pw = HBLand.hbVO.getPfLoginPw().getText();
				HBLand.memDTO = new MemberDAOImp().selectMember(id, pw);
				if(HBLand.memDTO!=null) { //로그인 성공
					JOptionPane.showMessageDialog(hbLand, "한빚랜드에 오신 것을 환영합니다.");
					MemberInfo memInfo = new MemberInfo(hbLand);
					GameSelect gameSelect = new GameSelect(hbLand);
					//info에 코인 출력
					LogDTO coinDTO = new LogDAOImp().selectCoin(id);
					//info에 게임 기록 출력
					int selectWin = new LogDAOImp().selectResult(0, id);
					int selectLose = new LogDAOImp().selectResult(1, id);
					String resultStr = Integer.toString(selectWin)+"/"+Integer.toString(selectLose);
					HBLand.hbVO.lbInfoRecord.setText(resultStr);
					HBLand.hbVO.lbInfoCoin.setText(Integer.toString(coinDTO.getCoin()));
					//화면 설정
					hbLand.backgroundImg = new ImageIcon("src/mainImgs/lobbyImg.png").getImage();
					HBLand.hbVO.btnJoin.setVisible(false);
					HBLand.hbVO.getBtnLogin().setVisible(false);
					HBLand.hbVO.getTfLoginId().setVisible(false);
					HBLand.hbVO.getPfLoginPw().setVisible(false);
				} else {JOptionPane.showMessageDialog(hbLand, "아이디 또는 비밀번호를 확인해주세요.");}
			}else {JOptionPane.showMessageDialog(hbLand, "아이디 또는 비밀번호를 입력해주세요.");}
		}
	}
}
